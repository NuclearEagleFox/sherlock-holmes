# Sherlock Holmes and the Three Mysteries
`Codename: SHUDGE`  
`Current Version: ???`

A game created for Awful Summer Jam 2017. Built using [HaxeFlixel](https://github.com/HaxeFlixel/flixel "Haxeflixel Github Repository").

### Building & Running
This project was made with a very certain HaxeDevelop/Haxelib toolchain that has been lost to time. Good luck.

### Status & Contribution
At this time, I consider this project complete and will likely not be updating or contributing to it any more. Forks are therefore preferred to pull requests.

### Roadmap
A guide to planned features and future versions.

## Related Project(s)
https://github.com/HaxeFlixel/flixel

package;

import flixel.FlxG;
import flixel.FlxState;
import flixel.math.FlxMath;
import flixel.text.FlxText;
import flixel.tweens.FlxTween;
import flixel.util.FlxColor;

enum OptionsSubState {
	
	BACK_SELECTED;
	LIGHTNING_SELECTED;
	VOLUME_SELECTED;
	
}

class OptionsState extends FlxState {
	
	var character:CharacterInfo;
	
	var backText:FlxText;
	var lightningText:FlxText;
	var lightningSettingText:FlxText;
	var titleText:FlxText;
	var volumeText: FlxText;
	var volumeSettingText:FlxText;
	
	var lightningGap:Int = 5;
	var lightningSettingGap:Int = 20;
	var titleGap:Int = 60;
	var volumeGap:Int = 5;
	var volumeSettingGap:Int = 20;
	
	var settings:SettingsInfo;
	
	override public function create():Void {
		
		FlxG.log.add("Options State");
		
		backText = new FlxText(0, 0, "Back");
		backText.setFormat(null, 18, FlxColor.fromRGB(255, 215, 0), FlxTextAlign.CENTER, FlxTextBorderStyle.OUTLINE, FlxColor.BLACK);
		backText.setBorderStyle(FlxTextBorderStyle.OUTLINE, FlxColor.BLACK, 3);
		backText.x = (FlxG.width - backText.width) / 2;
		
		lightningText = new FlxText(0, 0, "Lightning Effects");
		lightningText.setFormat(null, 18, FlxColor.fromRGB(255, 215, 0), FlxTextAlign.CENTER, FlxTextBorderStyle.OUTLINE, FlxColor.BLACK);
		lightningText.setBorderStyle(FlxTextBorderStyle.OUTLINE, FlxColor.BLACK, 3);
		lightningText.x = (FlxG.width - lightningText.width) / 2;
		
		lightningSettingText = new FlxText(0, 0, settings.lightningEffects ? "On" : "Off");
		lightningSettingText.setFormat(null, 18, FlxColor.fromRGB(255, 215, 0), FlxTextAlign.CENTER, FlxTextBorderStyle.OUTLINE, FlxColor.BLACK);
		lightningSettingText.setBorderStyle(FlxTextBorderStyle.OUTLINE, FlxColor.BLACK, 3);
		lightningSettingText.x = (FlxG.width - lightningSettingText.width) / 2;
		
		titleText = new FlxText(0, 0, "Options");
		titleText.setFormat(null, 20, FlxColor.fromRGB(255, 215, 0), FlxTextAlign.CENTER, FlxTextBorderStyle.OUTLINE, FlxColor.BLACK);
		titleText.setBorderStyle(FlxTextBorderStyle.OUTLINE, FlxColor.BLACK, 3);
		titleText.x = (FlxG.width - titleText.width) / 2;
		
		volumeText = new FlxText(0, 0, "Volume");
		volumeText.setFormat(null, 18, FlxColor.fromRGB(255, 215, 0), FlxTextAlign.CENTER, FlxTextBorderStyle.OUTLINE, FlxColor.BLACK);
		volumeText.setBorderStyle(FlxTextBorderStyle.OUTLINE, FlxColor.BLACK, 3);
		volumeText.x = (FlxG.width - volumeText.width) / 2;
		
		volumeSettingText = new FlxText(0, 0, "" + (settings.volume * 10));
		volumeSettingText.setFormat(null, 18, FlxColor.fromRGB(255, 215, 0), FlxTextAlign.CENTER, FlxTextBorderStyle.OUTLINE, FlxColor.BLACK);
		volumeSettingText.setBorderStyle(FlxTextBorderStyle.OUTLINE, FlxColor.BLACK, 3);
		volumeSettingText.x = (FlxG.width - volumeSettingText.width) / 2;
		
		titleText.y = (FlxG.height - (titleText.height + titleGap + volumeText.height + volumeGap + volumeSettingText.height + volumeSettingGap + lightningText.height + lightningGap + lightningSettingText.height + lightningSettingGap + backText.height)) / 2;
		volumeText.y = titleText.y + titleText.height + titleGap;
		volumeSettingText.y = volumeText.y + volumeText.height + volumeGap;
		lightningText.y = volumeSettingText.y + volumeSettingText.height + volumeSettingGap;
		lightningSettingText.y = lightningText.y + lightningText.height + lightningGap;
		backText.y = lightningSettingText.y + lightningSettingText.height + lightningSettingGap;
		
		character.watsonSprite.x = -character.watsonSprite.width;
		character.watsonSprite.y = FlxG.height - character.watsonSprite.height;
		
		add(settings.rainer4);
		add(settings.rainer3);
		add(character.watsonSprite);
		add(titleText);
		add(volumeText);
		add(volumeSettingText);
		add(lightningText);
		add(lightningSettingText);
		add(backText);
		add(settings.rainer2);
		add(settings.rainer1);
		add(settings.floor);
		
		FlxG.sound.play(AssetPaths.rain04__ogg, settings.volume, true);
		
		FlxG.camera.fade(FlxColor.BLACK, 0.2, true);
		
		super.create();
		
	}

	override public function new(c:CharacterInfo, s:SettingsInfo) {
		
		super();
		
		character = c;
		
		settings = s;
		
	}
	
	private function switchToMainMenuState():Void {
		
		remove(character.watsonSprite);
		remove(settings.floor);
		remove(settings.rainer1);
		remove(settings.rainer2);
		remove(settings.rainer3);
		remove(settings.rainer4);
		FlxG.switchState(new MainMenuState(character, settings));
		
	}
	
	override public function update(elapsed:Float):Void {
		
		if (settings.lightningEffects) {
		
			settings.flashCounter -= elapsed;
			if (settings.flashCounter < 0) {
				
				switch (settings.random.int(1, 6)) {
					
					case 1:
						FlxG.sound.play(AssetPaths.lightning01__ogg, settings.volume);
						
					case 2:
						FlxG.sound.play(AssetPaths.lightning02__ogg, settings.volume);
						
					case 3:
						FlxG.sound.play(AssetPaths.lightning03__ogg, settings.volume);
						
					case 4:
						FlxG.sound.play(AssetPaths.lightning04__ogg, settings.volume);
						
					case 5:
						FlxG.sound.play(AssetPaths.lightning05__ogg, settings.volume);
						
					case 6:
						FlxG.sound.play(AssetPaths.lightning06__ogg, settings.volume);
					
				}
				
				FlxG.camera.flash(FlxColor.WHITE, settings.random.float(0.1, 1.0));
				FlxG.camera.shake(0.001);
				settings.flashCounter = settings.random.float(1, 20);
				
			}
			
		}
		
		settings.characterCounter -= elapsed;
		if (settings.characterCounter < 0) {
			
			if (character.watsonSprite.animation.curAnim != null)
				character.watsonSprite.animation.finish();
			settings.characterCounter = settings.random.float(6, 60);
			
			if (settings.random.bool()) {
				
				character.watsonSprite.x = 0 - character.watsonSprite.width;
				character.watsonSprite.y = FlxG.height - character.watsonSprite.height;
				character.watsonSprite.animation.play("right");
				character.tween = FlxTween.tween(character.watsonSprite, {x: FlxG.width}, 5);
				
			} else {
				
				character.watsonSprite.x = FlxG.width;
				character.watsonSprite.y = FlxG.height - character.watsonSprite.height;
				character.watsonSprite.animation.play("left");
				character.tween = FlxTween.tween(character.watsonSprite, {x: (0 - character.watsonSprite.width)}, 5);
				
			}
			
		}
		
		lightningSettingText.text = "" + (settings.lightningEffects ? "On" : "Off");
		volumeSettingText.text = "" + (settings.volume * 10);
		
		if (settings.optionsState == OptionsSubState.BACK_SELECTED) {
			
			backText.setBorderStyle(FlxTextBorderStyle.OUTLINE, bgColor.getLightened(), 3);
			lightningSettingText.setBorderStyle(FlxTextBorderStyle.OUTLINE, FlxColor.BLACK, 3);
			volumeSettingText.setBorderStyle(FlxTextBorderStyle.OUTLINE, FlxColor.BLACK, 3);
			
			if (FlxG.keys.anyJustPressed([UP, W])) {
				
				settings.optionsState = OptionsSubState.LIGHTNING_SELECTED;
				
			} else if (FlxG.keys.anyJustPressed([DOWN, S])) {
				
				settings.optionsState = OptionsSubState.VOLUME_SELECTED;
				
			} else if (FlxG.keys.anyJustPressed([SPACE, ENTER])) {
				
				FlxG.camera.fade(FlxColor.BLACK, 0.2, false, switchToMainMenuState);
				
			}
			
		} else if (settings.optionsState == OptionsSubState.LIGHTNING_SELECTED) {
			
			backText.setBorderStyle(FlxTextBorderStyle.OUTLINE, FlxColor.BLACK, 3);
			lightningSettingText.setBorderStyle(FlxTextBorderStyle.OUTLINE, bgColor.getLightened(), 3);
			volumeSettingText.setBorderStyle(FlxTextBorderStyle.OUTLINE, FlxColor.BLACK, 3);
			
			if (FlxG.keys.anyJustPressed([UP, W])) {
				
				settings.optionsState = OptionsSubState.VOLUME_SELECTED;
				
			} else if (FlxG.keys.anyJustPressed([DOWN, S])) {
				
				settings.optionsState = OptionsSubState.BACK_SELECTED;
				
			} else if (FlxG.keys.anyJustPressed([LEFT, RIGHT, A, D])) {
				
				settings.lightningEffects = !settings.lightningEffects;
				
			}
			
			lightningSettingText.text = "< " + (settings.lightningEffects ? "On" : "Off") + " >";
			
		} else if (settings.optionsState == OptionsSubState.VOLUME_SELECTED) {
			
			backText.setBorderStyle(FlxTextBorderStyle.OUTLINE, FlxColor.BLACK, 3);
			lightningSettingText.setBorderStyle(FlxTextBorderStyle.OUTLINE, FlxColor.BLACK, 3);
			volumeSettingText.setBorderStyle(FlxTextBorderStyle.OUTLINE, bgColor.getLightened(), 3);
			
			if (FlxG.keys.anyJustPressed([LEFT, A])) {
				
				settings.volume -= 0.1;
				
			} else if (FlxG.keys.anyJustPressed([RIGHT, D])) {
				
				settings.volume += 0.1;
				
			} else if (FlxG.keys.anyJustPressed([UP, W])) {
				
				settings.optionsState = OptionsSubState.BACK_SELECTED;
				
			} else if (FlxG.keys.anyJustPressed([DOWN, S])) {
				
				settings.optionsState = OptionsSubState.LIGHTNING_SELECTED;
				
			}
			
			settings.volume = FlxMath.bound(settings.volume, 0.0, 1.0);
			settings.volume = FlxMath.roundDecimal(settings.volume, 1);
			volumeSettingText.text = (settings.volume == 0 ? "" : "< ") + (settings.volume * 10) + (settings.volume == 1.0 ? "" : " >");
			FlxG.sound.volume = settings.volume;
			
		}
		
		if (FlxG.keys.anyJustPressed([ESCAPE, BACKSPACE])) {
			
			FlxG.camera.fade(FlxColor.BLACK, 0.2, false, switchToMainMenuState);
			
		}
		
		lightningSettingText.x = (FlxG.width - lightningSettingText.width) / 2;
		volumeSettingText.x = (FlxG.width - volumeSettingText.width) / 2;
		
		FlxG.collide(settings.rainer1, settings.floor);
		FlxG.collide(settings.rainer2, settings.floor);
		FlxG.collide(settings.rainer3, settings.floor);
		FlxG.collide(settings.rainer4, settings.floor);
		
		super.update(elapsed);
		
	}
	
}
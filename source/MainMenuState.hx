package;

import flash.system.System;
import flixel.FlxG;
import flixel.FlxState;
import flixel.math.FlxMath;
import flixel.text.FlxText;
import flixel.tweens.FlxTween;
import flixel.util.FlxColor;
	
enum MainMenuSubState {
	
	EXIT_SELECTED;
	INTRO_1;
	INTRO_2;
	INTRO_3;
	INTRO_4;
	INTRO_5;
	INTRO_6;
	NEW_SELECTED;
	OPTIONS_SELECTED;
	
}

class MainMenuState extends FlxState {
	
	var character:CharacterInfo;
	
	var counter:Float = 0;
	var titleFinalY:Float;
	
	var danielsText:FlxText;
	var exitText:FlxText;
	var newGameText:FlxText;
	var optionsText:FlxText;
	var overwriteText:FlxText;
	var teamWorkText:FlxText;
	var titleText1:FlxText;
	var titleText2:FlxText;
	var titleText3:FlxText;
	var xText1:FlxText;
	var xText2:FlxText;
	var xText3:FlxText;
	var xText4:FlxText;
	var xText5:FlxText;
	
	var newGameGap:Int = 20;
	var optionsGap:Int = 20;
	var titleGap:Int = 80;
	
	var settings:SettingsInfo;

	override public function create():Void {
		
		FlxG.log.add("Main Menu State");
		
		bgColor = settings.backgroundColor;
		
		FlxG.mouse.visible = false;
		
		teamWorkText = new FlxText(0, 0, "Gemeinschafts Arbeit Spiele Präsentiert:");
		teamWorkText.setFormat(null, 20, FlxColor.fromRGB(255, 215, 0), FlxTextAlign.CENTER, FlxTextBorderStyle.OUTLINE, FlxColor.BLACK);
		teamWorkText.setBorderStyle(FlxTextBorderStyle.OUTLINE, FlxColor.BLACK, 3);
		teamWorkText.alpha = 0.0;
		teamWorkText.x = (FlxG.width - teamWorkText.width) / 2;
		teamWorkText.y = (FlxG.height - teamWorkText.height) / 2;
		
		xText1 = new FlxText(0, 0, "X");
		xText1.setFormat(null, 40, FlxColor.RED, FlxTextAlign.CENTER, FlxTextBorderStyle.OUTLINE, FlxColor.BLACK);
		xText1.setBorderStyle(FlxTextBorderStyle.OUTLINE, FlxColor.BLACK, 3);
		xText1.alpha = 0.0;
		xText1.x = teamWorkText.x + (teamWorkText.width / 5);
		xText1.y = teamWorkText.y - ((xText1.height - teamWorkText.height) / 2);
		
		xText2 = new FlxText(0, 0, "X");
		xText2.setFormat(null, 40, FlxColor.RED, FlxTextAlign.CENTER, FlxTextBorderStyle.OUTLINE, FlxColor.BLACK);
		xText2.setBorderStyle(FlxTextBorderStyle.OUTLINE, FlxColor.BLACK, 3);
		xText2.alpha = 0.0;
		xText2.x = teamWorkText.x + (teamWorkText.width * 2 / 5);
		xText2.y = teamWorkText.y - ((xText2.height - teamWorkText.height) / 2);
		
		xText3 = new FlxText(0, 0, "X");
		xText3.setFormat(null, 40, FlxColor.RED, FlxTextAlign.CENTER, FlxTextBorderStyle.OUTLINE, FlxColor.BLACK);
		xText3.setBorderStyle(FlxTextBorderStyle.OUTLINE, FlxColor.BLACK, 3);
		xText3.alpha = 0.0;
		xText3.x = teamWorkText.x + (teamWorkText.width * 3 / 5);
		xText3.y = teamWorkText.y - ((xText3.height - teamWorkText.height) / 2);
		
		xText4 = new FlxText(0, 0, "X");
		xText4.setFormat(null, 40, FlxColor.RED, FlxTextAlign.CENTER, FlxTextBorderStyle.OUTLINE, FlxColor.BLACK);
		xText4.setBorderStyle(FlxTextBorderStyle.OUTLINE, FlxColor.BLACK, 3);
		xText4.alpha = 0.0;
		xText4.x = teamWorkText.x + (teamWorkText.width * 4 / 5);
		xText4.y = teamWorkText.y - ((xText4.height - teamWorkText.height) / 2);
		
		danielsText = new FlxText(0, 0, "Daniel's Bootleg Translations Presents:");
		danielsText.setFormat(null, 20, FlxColor.fromRGB(255, 215, 0), FlxTextAlign.CENTER, FlxTextBorderStyle.OUTLINE, FlxColor.BLACK);
		danielsText.setBorderStyle(FlxTextBorderStyle.OUTLINE, FlxColor.BLACK, 3);
		danielsText.alpha = 0.0;
		danielsText.x = (FlxG.width - danielsText.width) / 2;
		danielsText.y = teamWorkText.y + teamWorkText.height + 5;
		
		titleText1 = new FlxText(0, 0, "Sherlock Holmes and the");
		titleText1.setFormat(null, 20, FlxColor.fromRGB(255, 215, 0), FlxTextAlign.CENTER, FlxTextBorderStyle.OUTLINE, FlxColor.BLACK);
		titleText1.setBorderStyle(FlxTextBorderStyle.OUTLINE, FlxColor.BLACK, 3);
		titleText1.alpha = 0.0;
		titleText1.moves = true;
		
		titleText2 = new FlxText(0, 0, " Three ");
		titleText2.setFormat(null, 20, FlxColor.fromRGB(255, 215, 0), FlxTextAlign.CENTER, FlxTextBorderStyle.OUTLINE, FlxColor.BLACK);
		titleText2.setBorderStyle(FlxTextBorderStyle.OUTLINE, FlxColor.BLACK, 3);
		titleText2.alpha = 0.0;
		titleText2.moves = true;
		
		titleText3 = new FlxText(0, 0, "Mysteries");
		titleText3.setFormat(null, 20, FlxColor.fromRGB(255, 215, 0), FlxTextAlign.CENTER, FlxTextBorderStyle.OUTLINE, FlxColor.BLACK);
		titleText3.setBorderStyle(FlxTextBorderStyle.OUTLINE, FlxColor.BLACK, 3);
		titleText3.alpha = 0.0;
		titleText3.moves = true;
		
		titleText1.x = (FlxG.width - (titleText1.width + titleText2.width + titleText3.width)) / 2;
		titleText2.x = titleText1.x + titleText1.width;
		titleText3.x = titleText2.x + titleText2.width;
		titleText3.moves = true;
		
		xText5 = new FlxText(0, 0, "X");
		xText5.setFormat(null, 40, FlxColor.RED, FlxTextAlign.CENTER, FlxTextBorderStyle.OUTLINE, FlxColor.BLACK);
		xText5.setBorderStyle(FlxTextBorderStyle.OUTLINE, FlxColor.BLACK, 3);
		xText5.x = titleText2.x + ((titleText2.width - xText5.width) / 2);
		xText5.alpha = 0.0;
		xText5.moves = true;
		
		overwriteText = new FlxText(0, 0, "Four");
		overwriteText.setFormat(null, 20, FlxColor.fromRGB(255, 215, 0), FlxTextAlign.CENTER, FlxTextBorderStyle.OUTLINE, FlxColor.BLACK);
		overwriteText.setBorderStyle(FlxTextBorderStyle.OUTLINE, FlxColor.BLACK, 3);
		overwriteText.x = titleText2.x + ((titleText2.width - overwriteText.width) / 2);
		overwriteText.alpha = 0.0;
		overwriteText.moves = true;
		
		newGameText = new FlxText(0, 0, "Start");
		newGameText.setFormat(null, 18, FlxColor.fromRGB(255, 215, 0), FlxTextAlign.CENTER, FlxTextBorderStyle.OUTLINE, FlxColor.BLACK);
		newGameText.setBorderStyle(FlxTextBorderStyle.OUTLINE, FlxColor.BLACK, 3);
		newGameText.x = (FlxG.width - newGameText.width) / 2;
		newGameText.alpha = 0.0;
		
		optionsText = new FlxText(0, 0, "Options");
		optionsText.setFormat(null, 18, FlxColor.fromRGB(255, 215, 0), FlxTextAlign.CENTER, FlxTextBorderStyle.OUTLINE, FlxColor.BLACK);
		optionsText.setBorderStyle(FlxTextBorderStyle.OUTLINE, FlxColor.BLACK, 3);
		optionsText.x = (FlxG.width - optionsText.width) / 2;
		optionsText.alpha = 0.0;
		
		exitText = new FlxText(0, 0, "Exit");
		exitText.setFormat(null, 18, FlxColor.fromRGB(255, 215, 0), FlxTextAlign.CENTER, FlxTextBorderStyle.OUTLINE, FlxColor.BLACK);
		exitText.setBorderStyle(FlxTextBorderStyle.OUTLINE, FlxColor.BLACK, 3);
		exitText.x = (FlxG.width - exitText.width) / 2;
		exitText.alpha = 0.0;
		
		titleText1.y = (FlxG.height - titleText1.height) / 2;
		titleFinalY = (FlxG.height - (titleText1.height + titleGap + newGameText.height + newGameGap + optionsText.height + optionsGap + exitText.height)) / 2;
		titleText2.y = titleText1.y;
		titleText3.y = titleText1.y;
		xText5.y = titleText2.y - ((xText5.height - titleText2.height) / 2);
		overwriteText.y = titleText2.y + titleText2.height + 5;
		newGameText.y = titleFinalY + titleText1.height + titleGap;
		optionsText.y = newGameText.y + newGameText.height + newGameGap;
		exitText.y = optionsText.y + optionsText.height + optionsGap;
		
		character.watsonSprite.x = -character.watsonSprite.width;
		character.watsonSprite.y = FlxG.height - character.watsonSprite.height;
		
		add(settings.rainer4);
		add(settings.rainer3);
		add(character.watsonSprite);
		add(teamWorkText);
		add(danielsText);
		add(titleText1);
		add(titleText2);
		add(titleText3);
		add(xText1);
		add(xText2);
		add(xText3);
		add(xText4);
		add(xText5);
		add(overwriteText);
		add(newGameText);
		add(optionsText);
		add(exitText);
		add(settings.rainer2);
		add(settings.rainer1);
		add(settings.floor);
		
		if (FlxG.sound.music == null) {
			
			settings.currentSong = "Suspense Celeste";
			FlxG.sound.playMusic(AssetPaths.Suspense_Celeste__ogg, settings.volume, true);
			
		}
		
		FlxG.sound.play(AssetPaths.rain04__ogg, settings.volume, true);
		
		if (settings.initialStart) {
			
			FlxG.camera.fade(FlxColor.BLACK, 0.2, true);
			settings.initialStart = false;
			
		} else {
			
			titleText1.y = titleFinalY;
			titleText2.y = titleText1.y;
			titleText3.y = titleText1.y;
			xText5.y = titleText2.y - ((xText5.height - titleText2.height) / 2);
			overwriteText.y = titleText2.y + titleText2.height + 5;
			newGameText.y = titleFinalY + titleText1.height + titleGap;
			optionsText.y = newGameText.y + newGameText.height + newGameGap;
			exitText.y = optionsText.y + optionsText.height + optionsGap;
			titleText1.alpha = 1.0;
			titleText2.alpha = 1.0;
			titleText3.alpha = 1.0;
			xText5.alpha = 1.0;
			overwriteText.alpha = 1.0;
			newGameText.alpha = 1.0;
			optionsText.alpha = 1.0;
			exitText.alpha = 1.0;
			FlxG.camera.fade(FlxColor.BLACK, 0.2, true);
			
		}
		
		super.create();
		
	}
	
	override public function new(c:CharacterInfo, s:SettingsInfo):Void {
		
		super();
		
		character = c;
		
		settings = s;
		
	}
	
	private function switchToExit():Void {
		
		System.exit(0);
		
	}
	
	private function switchToMysterySelectState():Void {
		
		remove(character.watsonSprite);
		remove(settings.floor);
		remove(settings.rainer1);
		remove(settings.rainer2);
		remove(settings.rainer3);
		remove(settings.rainer4);
		FlxG.switchState(new MysterySelectState(character, settings));
		
	}
	
	private function switchToOptionsState():Void {
		
		remove(character.watsonSprite);
		remove(settings.floor);
		remove(settings.rainer1);
		remove(settings.rainer2);
		remove(settings.rainer3);
		remove(settings.rainer4);		
		FlxG.switchState(new OptionsState(character, settings));
		
	}

	override public function update(elapsed:Float):Void {
		
		if (settings.lightningEffects) {
		
			settings.flashCounter -= elapsed;
			if (settings.flashCounter < 0) {
				
				switch (settings.random.int(1, 6)) {
					
					case 1:
						FlxG.sound.play(AssetPaths.lightning01__ogg, settings.volume);
						
					case 2:
						FlxG.sound.play(AssetPaths.lightning02__ogg, settings.volume);
						
					case 3:
						FlxG.sound.play(AssetPaths.lightning03__ogg, settings.volume);
						
					case 4:
						FlxG.sound.play(AssetPaths.lightning04__ogg, settings.volume);
						
					case 5:
						FlxG.sound.play(AssetPaths.lightning05__ogg, settings.volume);
						
					case 6:
						FlxG.sound.play(AssetPaths.lightning06__ogg, settings.volume);
					
				}
				
				FlxG.camera.flash(FlxColor.WHITE, settings.random.float(0.1, 1.0));
				FlxG.camera.shake(0.001);
				settings.flashCounter = settings.random.float(1, 20);
				
			}
			
		}
		
		settings.characterCounter -= elapsed;
		if (settings.characterCounter < 0) {
			
			if (character.watsonSprite.animation.curAnim != null)
				character.watsonSprite.animation.finish();
			settings.characterCounter = settings.random.float(6, 60);
			
			if (settings.random.bool()) {
				
				character.watsonSprite.x = 0 - character.watsonSprite.width;
				character.watsonSprite.y = FlxG.height - character.watsonSprite.height;
				character.watsonSprite.animation.play("right");
				character.tween = FlxTween.tween(character.watsonSprite, {x: FlxG.width}, 5);
				
			} else {
				
				character.watsonSprite.x = FlxG.width;
				character.watsonSprite.y = FlxG.height - character.watsonSprite.height;
				character.watsonSprite.animation.play("left");
				character.tween = FlxTween.tween(character.watsonSprite, {x: (0 - character.watsonSprite.width)}, 5);
				
			}
			
		}
		
		if (settings.mainMenuState == MainMenuSubState.EXIT_SELECTED) {
			
			exitText.setBorderStyle(FlxTextBorderStyle.OUTLINE, bgColor.getLightened(), 3);
			newGameText.setBorderStyle(FlxTextBorderStyle.OUTLINE, FlxColor.BLACK, 3);
			optionsText.setBorderStyle(FlxTextBorderStyle.OUTLINE, FlxColor.BLACK, 3);
			
			if (FlxG.keys.anyJustPressed([UP, W])) {
				
				settings.mainMenuState = MainMenuSubState.OPTIONS_SELECTED;
				
			} else if (FlxG.keys.anyJustPressed([DOWN, S])) {
				
				settings.mainMenuState = MainMenuSubState.NEW_SELECTED;
				
			} else if (FlxG.keys.anyJustPressed([SPACE, ENTER])) {
				
				FlxG.camera.fade(FlxColor.BLACK, 0.2, false, switchToExit);
				
			}
			
		} else if (settings.mainMenuState == MainMenuSubState.INTRO_1) {
			
			teamWorkText.alpha += 0.005;
			teamWorkText.alpha = FlxMath.bound(teamWorkText.alpha, 0.0, 1.0);
			
			if (FlxG.keys.anyJustPressed([SPACE, ENTER]) || teamWorkText.alpha == 1.0) {
				
				teamWorkText.alpha = 1.0;
				settings.mainMenuState = MainMenuSubState.INTRO_2;
				
			}
			
		} else if (settings.mainMenuState == MainMenuSubState.INTRO_2) {
			
			danielsText.alpha += 0.005;
			danielsText.alpha = FlxMath.bound(danielsText.alpha, 0.0, 1.0);
			
			xText1.alpha += 0.005;
			xText1.alpha = FlxMath.bound(xText1.alpha, 0.0, 1.0);
			
			xText2.alpha += 0.005;
			xText2.alpha = FlxMath.bound(xText2.alpha, 0.0, 1.0);
			
			xText3.alpha += 0.005;
			xText3.alpha = FlxMath.bound(xText3.alpha, 0.0, 1.0);
			
			xText4.alpha += 0.005;
			xText4.alpha = FlxMath.bound(xText4.alpha, 0.0, 1.0);
			
			if (danielsText.alpha == 1.0)
				counter += elapsed;
			
			if (FlxG.keys.anyJustPressed([SPACE, ENTER]) || counter >= 1.0) {
				
				danielsText.alpha = 1.0;
				xText1.alpha = 1.0;
				xText2.alpha = 1.0;
				xText3.alpha = 1.0;
				xText4.alpha = 1.0;
				settings.mainMenuState = MainMenuSubState.INTRO_3;
				
			}
			
		} else if (settings.mainMenuState == MainMenuSubState.INTRO_3) {
			
			teamWorkText.alpha -= 0.005;
			danielsText.alpha -= 0.005;
			teamWorkText.alpha = FlxMath.bound(teamWorkText.alpha, 0.0, 1.0);
			danielsText.alpha = FlxMath.bound(danielsText.alpha, 0.0, 1.0);
			
			xText1.alpha -= 0.005;
			xText1.alpha = FlxMath.bound(xText1.alpha, 0.0, 1.0);
			
			xText2.alpha -= 0.005;
			xText2.alpha = FlxMath.bound(xText2.alpha, 0.0, 1.0);
			
			xText3.alpha -= 0.005;
			xText3.alpha = FlxMath.bound(xText3.alpha, 0.0, 1.0);
			
			xText4.alpha -= 0.005;
			xText4.alpha = FlxMath.bound(xText4.alpha, 0.0, 1.0);
			
			if (FlxG.keys.anyJustPressed([SPACE, ENTER]) || (danielsText.alpha == 0.0 && teamWorkText.alpha == 0.0)) {
				
				danielsText.alpha = 0.0;
				teamWorkText.alpha = 0.0;
				xText1.alpha = 0.0;
				xText2.alpha = 0.0;
				xText3.alpha = 0.0;
				xText4.alpha = 0.0;
				settings.mainMenuState = MainMenuSubState.INTRO_4;
				
			}
			
		} else if (settings.mainMenuState == MainMenuSubState.INTRO_4) {
			
			titleText1.alpha += 0.005;
			titleText2.alpha += 0.005;
			titleText3.alpha += 0.005;
			xText5.alpha += 0.005;
			overwriteText.alpha += 0.005;
			titleText1.alpha = FlxMath.bound(titleText1.alpha, 0.0, 1.0);
			titleText2.alpha = FlxMath.bound(titleText2.alpha, 0.0, 1.0);
			titleText3.alpha = FlxMath.bound(titleText3.alpha, 0.0, 1.0);
			xText5.alpha = FlxMath.bound(xText5.alpha, 0.0, 1.0);
			overwriteText.alpha = FlxMath.bound(overwriteText.alpha, 0.0, 1.0);
			
			if (FlxG.keys.anyJustPressed([SPACE, ENTER]) || titleText1.alpha == 1.0) {
				
				titleText1.alpha = 1.0;
				titleText2.alpha = 1.0;
				titleText3.alpha = 1.0;
				xText5.alpha = 1.0;
				overwriteText.alpha = 1.0;
				settings.mainMenuState = MainMenuSubState.INTRO_5;
				
			}
			
		} else if (settings.mainMenuState == MainMenuSubState.INTRO_5) {
			
			titleText1.velocity.y = -50;
			titleText2.velocity.y = -50;
			titleText3.velocity.y = -50;
			xText5.velocity.y = -50;
			overwriteText.velocity.y = -50;
			
			if (FlxG.keys.anyJustPressed([SPACE, ENTER]) || titleText1.y <= titleFinalY) {
				
				titleText1.velocity.y = 0;
				titleText2.velocity.y = 0;
				titleText3.velocity.y = 0;
				xText5.velocity.y = 0;
				overwriteText.velocity.y = 0;
				titleText1.y = titleFinalY;
				titleText2.y = titleText1.y;
				titleText3.y = titleText1.y;
				xText5.y = titleText2.y - ((xText5.height - titleText2.height) / 2);
				overwriteText.y = titleText2.y + titleText2.height + 5;
				newGameText.y = titleFinalY + titleText1.height + titleGap;
				optionsText.y = newGameText.y + newGameText.height + newGameGap;
				exitText.y = optionsText.y + optionsText.height + optionsGap;
				settings.mainMenuState = MainMenuSubState.INTRO_6;
				
			}
			
		} else if (settings.mainMenuState == MainMenuSubState.INTRO_6) {
			
			newGameText.alpha += 0.005;
			optionsText.alpha += 0.005;
			exitText.alpha += 0.005;
			
			newGameText.alpha = FlxMath.bound(newGameText.alpha, 0.0, 1.0);
			optionsText.alpha = FlxMath.bound(optionsText.alpha, 0.0, 1.0);
			exitText.alpha = FlxMath.bound(exitText.alpha, 0.0, 1.0);
			
			if (FlxG.keys.anyJustPressed([SPACE, ENTER]) || newGameText.alpha == 1.0) {
				
				newGameText.alpha = 1.0;
				optionsText.alpha = 1.0;
				exitText.alpha = 1.0;
				settings.mainMenuState = MainMenuSubState.NEW_SELECTED;
				
			}
			
		} else if (settings.mainMenuState == MainMenuSubState.NEW_SELECTED) {
			
			exitText.setBorderStyle(FlxTextBorderStyle.OUTLINE, FlxColor.BLACK, 3);
			newGameText.setBorderStyle(FlxTextBorderStyle.OUTLINE, bgColor.getLightened(), 3);
			optionsText.setBorderStyle(FlxTextBorderStyle.OUTLINE, FlxColor.BLACK, 3);
			
			if (FlxG.keys.anyJustPressed([UP, W])) {
				
				settings.mainMenuState = MainMenuSubState.EXIT_SELECTED;
				
			} else if (FlxG.keys.anyJustPressed([DOWN, S])) {
				
				settings.mainMenuState = MainMenuSubState.OPTIONS_SELECTED;
				
			} else if (FlxG.keys.anyJustPressed([SPACE, ENTER])) {
				
				FlxG.camera.fade(FlxColor.BLACK, 0.2, false, switchToMysterySelectState);
				
			}
			
		} else if (settings.mainMenuState == MainMenuSubState.OPTIONS_SELECTED) {
			
			exitText.setBorderStyle(FlxTextBorderStyle.OUTLINE, FlxColor.BLACK, 3);
			newGameText.setBorderStyle(FlxTextBorderStyle.OUTLINE, FlxColor.BLACK, 3);
			optionsText.setBorderStyle(FlxTextBorderStyle.OUTLINE, bgColor.getLightened(), 3);
			
			if (FlxG.keys.anyJustPressed([UP, W])) {
				
				settings.mainMenuState = MainMenuSubState.NEW_SELECTED;
				
			} else if (FlxG.keys.anyJustPressed([DOWN, S])) {
				
				settings.mainMenuState = MainMenuSubState.EXIT_SELECTED;
				
			} else if (FlxG.keys.anyJustPressed([SPACE, ENTER])) {
				
				FlxG.camera.fade(FlxColor.BLACK, 0.2, false, switchToOptionsState);
				
			}
			
		}
		
		if (FlxG.keys.anyJustPressed([ESCAPE])) {
			
			FlxG.camera.fade(FlxColor.BLACK, 0.2, false, switchToExit);
			
		}
		
		FlxG.collide(settings.rainer1, settings.floor);
		FlxG.collide(settings.rainer2, settings.floor);
		FlxG.collide(settings.rainer3, settings.floor);
		FlxG.collide(settings.rainer4, settings.floor);
		
		super.update(elapsed);
		
	}
	
}
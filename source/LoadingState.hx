package;

import flixel.FlxG;
import flixel.FlxObject;
import flixel.FlxState;
import flixel.text.FlxText;
import flixel.util.FlxColor;

class LoadingState extends FlxState {
	
	var lines:Array<String>;
	
	var character:CharacterInfo;
	
	var loadingCounter:Float;
	
	var loadingText:FlxText;
	var speechText:FlxText;
	
	var settings:SettingsInfo;
	
	override public function create():Void {
		
		FlxG.log.add("Loading State");
		
		FlxG.worldBounds.set(0, 0, FlxG.width + 1, FlxG.height + 1);
		FlxG.camera.scroll.x = 0;
		FlxG.camera.scroll.y = 0;
		settings.floor.x = 0;
		settings.floor.y = FlxG.height;
		
		lines = new Array();
		
		loadingCounter = settings.random.float(5, 10);
		
		loadingText = new FlxText(0, 0, "Loading...");
		loadingText.setFormat(null, 30, FlxColor.fromRGB(255, 215, 0), FlxTextAlign.CENTER, FlxTextBorderStyle.OUTLINE, FlxColor.BLACK);
		loadingText.setBorderStyle(FlxTextBorderStyle.OUTLINE, FlxColor.BLACK, 3);
		loadingText.x = (FlxG.width - loadingText.width) / 2;
		loadingText.y = (FlxG.height - loadingText.height) / 2;
		
		character.danielSprite.x = (FlxG.width - (character.danielSprite.width + character.bubbleSprite.width)) / 2;
		character.danielSprite.y = FlxG.height - character.danielSprite.height;
		
		character.bubbleSprite.alpha = 1;
		character.bubbleSprite.x = character.danielSprite.x + character.danielSprite.width;
		character.bubbleSprite.y = FlxG.height - character.bubbleSprite.height - 5;
		
		if (character.location == "Credits")
			character.currentMystery = CharacterInfo.Mystery.CREDITS;
		
		var filename:String = "";
		switch (character.currentMystery) {
			
			case CharacterInfo.Mystery.MYSTERY1:
				filename = AssetPaths.LoadingScreenLines1__txt;
				
			case CharacterInfo.Mystery.MYSTERY2:
				filename = AssetPaths.LoadingScreenLines2__txt;
				
			case CharacterInfo.Mystery.MYSTERY3:
				filename = AssetPaths.LoadingScreenLines2__txt;
				
			case CharacterInfo.Mystery.MYSTYER4:
				filename = AssetPaths.LoadingScreenLines3__txt;
				
			case CharacterInfo.Mystery.CREDITS:
				filename = AssetPaths.LoadingScreenLines4__txt;
				
			default:
				filename = AssetPaths.LoadingScreenLines1__txt;
			
		}
		
		var fin = sys.io.File.read(filename, false);
		
		while (!fin.eof()) {
			
			var newLine:String = fin.readLine();
			lines.push(newLine);
			
		}
		
		var line:String = lines[settings.random.int(0, lines.length - 1)];
		
		speechText = new FlxText(0, 0, 515, line);
		speechText.setFormat(null, 15, FlxColor.fromRGB(9, 11, 34), FlxTextAlign.CENTER);
		speechText.x = character.bubbleSprite.x + 32;
		speechText.y = character.bubbleSprite.y + ((character.bubbleSprite.height - speechText.height) / 2);
		
		add(settings.rainer4);
		add(settings.rainer3);
		add(loadingText);
		add(character.danielSprite);
		add(character.bubbleSprite);
		add(speechText);
		add(settings.rainer2);
		add(settings.rainer1);
		add(settings.floor);
		
		FlxG.sound.play(AssetPaths.rain04__ogg, settings.volume, true);
		
		switch (character.location) {
			
			case "221B Baker Street":
				if (settings.currentSong != "Mystery Boulevard") {
					
					settings.currentSong = "Mystery Boulevard";
					FlxG.sound.playMusic(AssetPaths.Mystery_Boulevard__ogg, settings.volume, true);
					
				}
				
			case "Manor Hurlstone, Exterior":
				if (settings.currentSong != "Highland Holdfast") {
					
					settings.currentSong = "Highland Holdfast";
					FlxG.sound.playMusic(AssetPaths.Highland_Holdfast__ogg, settings.volume, true);
					
				}
				
			case "Mystery Select State":
				if (settings.currentSong != "Suspense Celeste") {
					
					settings.currentSong = "Suspense Celeste";
					FlxG.sound.playMusic(AssetPaths.Suspense_Celeste__ogg, settings.volume, true);
					
				}
				
			case "Shed, Secret Hole":
				if (settings.currentSong != "After the War") {
					
					settings.currentSong = "After the War";
					FlxG.sound.playMusic(AssetPaths.After_the_War__ogg, settings.volume, true);
					
				}
				
			case "Dark World":
				if (settings.currentSong != "Cyberspace") {
					
					settings.currentSong = "Cyberspace";
					FlxG.sound.playMusic(AssetPaths.Cyberspace__ogg, settings.volume, true);
					
				}
				
			case "Credits":
				if (settings.currentSong != "Streamline") {
					
					settings.currentSong = "Streamline";
					FlxG.sound.playMusic(AssetPaths.Streamline__ogg, settings.volume, true);
					
				}
				
			case "Main Menu State":
				if (settings.currentSong != "Suspense Celeste") {
					
					settings.currentSong = "Suspense Celeste";
					FlxG.sound.playMusic(AssetPaths.Suspense_Celeste__ogg, settings.volume, true);
					
				}
			
		}
		
		FlxG.camera.fade(FlxColor.BLACK, 0.2, true);
		
		super.create();
		
	}

	public function new(c:CharacterInfo, s:SettingsInfo) {
		
		super();
		
		character = c;
		
		settings = s;
		
	}
	
	private function switchToBakerStreetState():Void {
		
		clear();
		FlxG.switchState(new BakerStreetState(character, settings));
		
	}
	
	private function switchToCreditsState():Void {
		
		clear();
		FlxG.switchState(new CreditsState(character, settings));
		
	}
	
	private function switchToDarkWorldState():Void {
		
		clear();
		FlxG.switchState(new DarkWorldState(character, settings));
		
	}
	
	private function switchToHurlstoneExteriorState():Void {
		
		clear();
		FlxG.switchState(new HurlstoneExteriorState(character, settings));
		
	}
	
	private function switchToMainMenuState():Void {
		
		clear();
		FlxG.switchState(new MainMenuState(character, settings));
		
	}
	
	private function switchToMysterySelectState():Void {
		
		clear();
		FlxG.switchState(new MysterySelectState(character, settings));
		
	}
	
	private function switchToShedBasementState():Void {
		
		clear();
		FlxG.switchState(new ShedBasementState(character, settings));
		
	}
	
	private function switchToShedInteriorState():Void {
		
		clear();
		FlxG.switchState(new ShedInteriorState(character, settings));
		
	}
	
	override public function update(elapsed:Float):Void {
		
		if (settings.lightningEffects) {
			
			settings.flashCounter -= elapsed;
			if (settings.flashCounter < 0) {
				
				switch (settings.random.int(1, 6)) {
					
					case 1:
						FlxG.sound.play(AssetPaths.lightning01__ogg, settings.volume);
						
					case 2:
						FlxG.sound.play(AssetPaths.lightning02__ogg, settings.volume);
						
					case 3:
						FlxG.sound.play(AssetPaths.lightning03__ogg, settings.volume);
						
					case 4:
						FlxG.sound.play(AssetPaths.lightning04__ogg, settings.volume);
						
					case 5:
						FlxG.sound.play(AssetPaths.lightning05__ogg, settings.volume);
						
					case 6:
						FlxG.sound.play(AssetPaths.lightning06__ogg, settings.volume);
					
				}
				
				FlxG.camera.flash(FlxColor.WHITE, settings.random.float(0.1, 1.0));
				FlxG.camera.shake(0.001);
				settings.flashCounter = settings.random.float(1, 20);
				
			}
			
		}
		
		loadingCounter -= elapsed;
		
		if (loadingCounter < 0) {
			
			switch (character.location) {
				
				case "221B Baker Street":
					FlxG.camera.fade(FlxColor.BLACK, 0.2, false, switchToBakerStreetState);
					
				case "Manor Hurlstone, Exterior":
					FlxG.camera.fade(FlxColor.BLACK, 0.2, false, switchToHurlstoneExteriorState);
					
				case "Mystery Select State":
					FlxG.camera.fade(FlxColor.BLACK, 0.2, false, switchToMysterySelectState);
					
				case "Shed, Secret Hole":
					FlxG.camera.fade(FlxColor.BLACK, 0.2, false, switchToShedBasementState);
					
				case "Shed, Interior":
					FlxG.camera.fade(FlxColor.BLACK, 0.2, false, switchToShedInteriorState);
					
				case "Dark World":
					FlxG.camera.fade(FlxColor.BLACK, 0.2, false, switchToDarkWorldState);
					
				case "Credits":
					FlxG.camera.fade(FlxColor.BLACK, 0.2, false, switchToCreditsState);
					
				case "Main Menu State":
					FlxG.camera.fade(FlxColor.BLACK, 0.2, false, switchToMainMenuState);
				
			}
			
		}
		
		switch (character.danielSprite.facing) {
			
			case FlxObject.DOWN:
				character.danielSprite.animation.frameIndex = 1;
			
		}
		
		FlxG.collide(settings.rainer1, settings.floor);
		FlxG.collide(settings.rainer2, settings.floor);
		FlxG.collide(settings.rainer3, settings.floor);
		FlxG.collide(settings.rainer4, settings.floor);
		
		super.update(elapsed);
		
	}
	
}
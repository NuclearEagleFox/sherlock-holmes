package;

import flixel.FlxG;
import flixel.FlxSprite;
import flixel.effects.particles.FlxEmitter;
import flixel.math.FlxRandom;
import flixel.util.FlxColor;

class SettingsInfo {
	
	public var initialStart:Bool;
	public var lightningEffects:Bool;
	
	public var characterCounter:Float;
	public var flashCounter:Float;
	public var menuSpeed:Float = 0.4;
	public var volume:Float;
	
	public var backgroundColor:FlxColor;
	
	public var rainer1:FlxEmitter;
	public var rainer2:FlxEmitter;
	public var rainer3:FlxEmitter;
	public var rainer4:FlxEmitter;
	
	public var random:FlxRandom;
	
	public var floor:FlxSprite;
	
	public var gameMenuState:CharacterInfo.GameMenuState;
	
	public var creditsState:CreditsState.CreditsSubState;
	
	public var mainMenuState:MainMenuState.MainMenuSubState;
	
	public var mysterySelectState:MysterySelectState.MysterySelectSubState;
	
	public var optionsState:OptionsState.OptionsSubState;
	
	public var currentSong:String;

	public function new() {
		
		random = new FlxRandom();
		
		initialStart = true;
		
		lightningEffects = true;
		
		characterCounter = random.float(1, 60);
		
		flashCounter = random.float(1, 20);
		
		volume = 0.5;
		
		backgroundColor = FlxColor.fromRGB(9, 11, 34);
		
		rainer1 = new FlxEmitter(0, 0, 100);
		rainer1.makeParticles(6, 8, backgroundColor.getLightened().getLightened(), 100);
		rainer1.acceleration.start.max.y = 400;
		rainer1.acceleration.start.min.y = 400;
		rainer1.acceleration.end.max.y = 400;
		rainer1.acceleration.end.min.y = 400;
		rainer1.emitting = true;
		rainer1.height = 20;
		rainer1.launchMode = FlxEmitterMode.SQUARE;
		rainer1.lifespan.max = 10;
		rainer1.lifespan.min = 10;
		rainer1.solid = true;
		rainer1.velocity.start.max.x = 0;
		rainer1.velocity.start.min.x = 0;
		rainer1.velocity.end.max.x = 0;
		rainer1.velocity.end.min.x = 0;
		rainer1.width = FlxG.width;
		rainer1.x = 0;
		rainer1.y = -100;
		rainer1.start(false, 0.02);
		
		rainer2 = new FlxEmitter(0, 0, 100);
		rainer2.makeParticles(4, 6, backgroundColor.getLightened(), 100);
		rainer2.acceleration.start.max.y = 400;
		rainer2.acceleration.start.min.y = 400;
		rainer2.acceleration.end.max.y = 400;
		rainer2.acceleration.end.min.y = 400;
		rainer2.emitting = true;
		rainer2.height = 20;
		rainer2.launchMode = FlxEmitterMode.SQUARE;
		rainer2.lifespan.max = 10;
		rainer2.lifespan.min = 10;
		rainer2.solid = true;
		rainer2.velocity.start.max.x = 0;
		rainer2.velocity.start.min.x = 0;
		rainer2.velocity.end.max.x = 0;
		rainer2.velocity.end.min.x = 0;
		rainer2.width = FlxG.width;
		rainer2.x = 0;
		rainer2.y = -100;
		rainer2.start(false, 0.02);
		
		rainer3 = new FlxEmitter(0, 0, 100);
		rainer3.makeParticles(4, 6, backgroundColor.getDarkened().getDarkened(), 100);
		rainer3.acceleration.start.max.y = 400;
		rainer3.acceleration.start.min.y = 400;
		rainer3.acceleration.end.max.y = 400;
		rainer3.acceleration.end.min.y = 400;
		rainer3.emitting = true;
		rainer3.height = 20;
		rainer3.launchMode = FlxEmitterMode.SQUARE;
		rainer3.lifespan.max = 10;
		rainer3.lifespan.min = 10;
		rainer3.solid = true;
		rainer3.velocity.start.max.x = 0;
		rainer3.velocity.start.min.x = 0;
		rainer3.velocity.end.max.x = 0;
		rainer3.velocity.end.min.x = 0;
		rainer3.width = FlxG.width;
		rainer3.x = 0;
		rainer3.y = -100;
		rainer3.start(false, 0.02);
		
		rainer4 = new FlxEmitter(0, 0, 100);
		rainer4.makeParticles(2, 4, backgroundColor.getDarkened().getDarkened().getDarkened(), 100);
		rainer4.acceleration.start.max.y = 400;
		rainer4.acceleration.start.min.y = 400;
		rainer4.acceleration.end.max.y = 400;
		rainer4.acceleration.end.min.y = 400;
		rainer4.emitting = true;
		rainer4.height = 20;
		rainer4.launchMode = FlxEmitterMode.SQUARE;
		rainer4.lifespan.max = 10;
		rainer4.lifespan.min = 10;
		rainer4.solid = true;
		rainer4.velocity.start.max.x = 0;
		rainer4.velocity.start.min.x = 0;
		rainer4.velocity.end.max.x = 0;
		rainer4.velocity.end.min.x = 0;
		rainer4.width = FlxG.width;
		rainer4.x = 0;
		rainer4.y = -100;
		rainer4.start(false, 0.02);
		
		floor = new FlxSprite(0, FlxG.height);
		floor.makeGraphic(FlxG.width, 20, FlxColor.TRANSPARENT);
		floor.immovable = true;
		floor.solid = true;
		floor.elasticity = 0.8;
		
		gameMenuState = CharacterInfo.GameMenuState.RESUME_SELECTED;
		
		creditsState = CreditsState.CreditsSubState.CREATED_BY;
		
		mainMenuState = MainMenuState.MainMenuSubState.INTRO_1;
		
		mysterySelectState = MysterySelectState.MysterySelectSubState.MYSTERY_1_SELECTED;
		
		optionsState = OptionsState.OptionsSubState.VOLUME_SELECTED;
		
	}
	
}
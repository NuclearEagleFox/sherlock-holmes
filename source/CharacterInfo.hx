package;

import flixel.FlxG;
import flixel.util.FlxColor;
import flixel.FlxObject;
import flixel.FlxSprite;
import flixel.text.FlxText;
import flixel.tweens.FlxTween;

enum CharacterState {
	
	INTERACTING;
	MENU;
	MOVING;
	STATIONARY;
	
}

enum GameMenuState {
	
	EXIT_SELECTED;
	LOCATIONS_MENU_SELECTED;
	LOCATIONS_SELECTED;
	MAIN_MENU_SELECTED;
	RESUME_SELECTED;
	
}

enum Mystery {
	
	MYSTERY1;
	MYSTERY2;
	MYSTERY3;
	MYSTYER4;
	CREDITS;
	
}

class CharacterInfo {
	
	public var unlockedLocations:Array<String>;
	
	public var mystery4Unlocked:Bool = false;
	
	public var state:CharacterState;
	
	public var speed:Float = 0.16;
	
	public var nameText:FlxText;
	public var speechText:FlxText;
	
	public var tween:FlxTween;
	
	public var alertSprite:FlxSprite;
	public var bubbleSprite:FlxSprite;
	public var danielSprite:FlxSprite;
	public var watsonSprite:FlxSprite;
	
	public var currentMystery:Mystery;
	
	public var interactingWith:Prop;
	
	public var location:String;

	public function new() {
		
		unlockedLocations = new Array<String>();
		unlockedLocations.push("221B Baker Street");
		
		state = CharacterState.STATIONARY;
		
		nameText = new FlxText(0, 0, "[Placeholder]");
		nameText.setFormat(null, 9, FlxColor.fromRGB(255, 215, 0), FlxTextAlign.CENTER, FlxTextBorderStyle.OUTLINE, FlxColor.BLACK);
		nameText.setBorderStyle(FlxTextBorderStyle.OUTLINE, FlxColor.fromRGB(9, 11, 34).getLightened(), 1);
		
		speechText = new FlxText(0, 0, 515, "[Placeholder]");
		speechText.setFormat(null, 15, FlxColor.fromRGB(9, 11, 34), FlxTextAlign.CENTER);
		
		alertSprite = new FlxSprite();
		alertSprite.loadGraphic(AssetPaths.alert01__png, true, 36, 36);
		alertSprite.animation.add("alert", [0, 1, 2, 3, 4, 5, 6, 7, 8], 8, false);
		alertSprite.animation.frameIndex = 8;
		
		bubbleSprite = new FlxSprite(AssetPaths.bubble__png);
		
		danielSprite = new FlxSprite();
		danielSprite.loadGraphic(AssetPaths.daniel__png, true, 36, 52);
		danielSprite.facing = FlxObject.DOWN;
		danielSprite.animation.add("down", [0, 1, 2, 1], 5, true);
		danielSprite.animation.add("left", [3, 4, 5, 4], 5, true);
		danielSprite.animation.add("right", [8, 7, 6, 7], 5, true);
		danielSprite.animation.add("up", [9, 10, 11, 10], 5, true);
		danielSprite.height = 36;
		danielSprite.width = 36;
		danielSprite.offset.set(0, 16);
		
		watsonSprite = new FlxSprite();
		watsonSprite.loadGraphic(AssetPaths.watson__png, true, 36, 52);
		watsonSprite.animation.add("down", [0, 1, 2, 1], 5, true);
		watsonSprite.animation.add("left", [3, 4, 5, 4], 5, true);
		watsonSprite.animation.add("right", [8, 7, 6, 7], 5, true);
		watsonSprite.animation.add("up", [9, 10, 11, 10], 5, true);
		watsonSprite.facing = FlxObject.DOWN;
		watsonSprite.height = 36;
		watsonSprite.width = 36;
		watsonSprite.offset.set(0, 16);
		
	}
	
}
package;

import flixel.FlxG;
import flixel.FlxState;

class StartState extends FlxState {
	
	override public function create():Void {
		
		super.create();
		
	}

	override public function update(elapsed:Float):Void {
		
		FlxG.log.add("Start State");
		
		var character:CharacterInfo = new CharacterInfo();
		
		var settings:SettingsInfo = new SettingsInfo();
		
		FlxG.switchState(new MainMenuState(character, settings));
		
		super.update(elapsed);
		
	}
	
}
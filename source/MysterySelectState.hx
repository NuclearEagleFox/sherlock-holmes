package;

import flixel.FlxG;
import flixel.FlxState;
import flixel.text.FlxText;
import flixel.tweens.FlxTween;
import flixel.util.FlxColor;

enum MysterySelectSubState {
	
	BACK_SELECTED;
	MYSTERY_1_SELECTED;
	MYSTERY_2_SELECTED;
	MYSTERY_3_SELECTED;
	MYSTERY_4_SELECTED;
	
}

class MysterySelectState extends FlxState {
	
	var character:CharacterInfo;
	
	var backText:FlxText;
	var mystery1Text:FlxText;
	var mystery2Text:FlxText;
	var mystery3Text:FlxText;
	var mystery4Text:FlxText;
	var titleText:FlxText;
	var translationText1:FlxText;
	var translationText2:FlxText;
	
	var mystery1Gap:Int = 20;
	var mystery2Gap:Int = 20;
	var mystery3Gap:Int = 20;
	var mystery4Gap:Int = 20;
	var titleGap:Int = 60;
	
	var settings:SettingsInfo;
	
	override public function create():Void {
		
		FlxG.log.add("Mystery Select State");
		
		backText = new FlxText(0, 0, "Back");
		backText.setFormat(null, 18, FlxColor.fromRGB(255, 215, 0), FlxTextAlign.CENTER, FlxTextBorderStyle.OUTLINE, FlxColor.BLACK);
		backText.setBorderStyle(FlxTextBorderStyle.OUTLINE, FlxColor.BLACK, 3);
		backText.x = (FlxG.width - backText.width) / 2;
		
		mystery1Text = new FlxText(0, 0, "Dr. Watson and the Musgrave Ritual");
		mystery1Text.setFormat(null, 18, FlxColor.fromRGB(255, 215, 0), FlxTextAlign.CENTER, FlxTextBorderStyle.OUTLINE, FlxColor.BLACK);
		mystery1Text.setBorderStyle(FlxTextBorderStyle.OUTLINE, FlxColor.BLACK, 3);
		mystery1Text.x = (FlxG.width - mystery1Text.width) / 2;
		
		mystery2Text = new FlxText(0, 0, "Dr. Watson and the Missing Three-Quarter");
		mystery2Text.setFormat(null, 18, FlxColor.GRAY, FlxTextAlign.CENTER, FlxTextBorderStyle.OUTLINE, FlxColor.BLACK);
		mystery2Text.setBorderStyle(FlxTextBorderStyle.OUTLINE, FlxColor.BLACK, 3);
		mystery2Text.x = (FlxG.width - mystery2Text.width) / 2;
		
		translationText1 = new FlxText(0, 0, "[Translation Incomplete]");
		translationText1.setFormat(null, 10, FlxColor.GRAY, FlxTextAlign.CENTER, FlxTextBorderStyle.OUTLINE, FlxColor.BLACK);
		translationText1.setBorderStyle(FlxTextBorderStyle.OUTLINE, FlxColor.BLACK, 1);
		translationText1.x = (FlxG.width - translationText1.width) / 2;
		
		mystery3Text = new FlxText(0, 0, "Dr. Watson and the Dying Detective");
		mystery3Text.setFormat(null, 18, FlxColor.GRAY, FlxTextAlign.CENTER, FlxTextBorderStyle.OUTLINE, FlxColor.BLACK);
		mystery3Text.setBorderStyle(FlxTextBorderStyle.OUTLINE, FlxColor.BLACK, 3);
		mystery3Text.x = (FlxG.width - mystery3Text.width) / 2;
		
		translationText2 = new FlxText(0, 0, "[Translation Incomplete]");
		translationText2.setFormat(null, 10, FlxColor.GRAY, FlxTextAlign.CENTER, FlxTextBorderStyle.OUTLINE, FlxColor.BLACK);
		translationText2.setBorderStyle(FlxTextBorderStyle.OUTLINE, FlxColor.BLACK, 1);
		translationText2.x = (FlxG.width - translationText2.width) / 2;
		
		mystery4Text = new FlxText(0, 0, "Daniel and the Final Problem");
		mystery4Text.setFormat(null, 18, FlxColor.fromRGB(255, 215, 0), FlxTextAlign.CENTER, FlxTextBorderStyle.OUTLINE, FlxColor.BLACK);
		mystery4Text.setBorderStyle(FlxTextBorderStyle.OUTLINE, FlxColor.BLACK, 3);
		mystery4Text.x = (FlxG.width - mystery4Text.width) / 2;
		mystery4Text.alpha = 0;
		
		titleText = new FlxText(0, 0, "Choose a Mystery to Solve:");
		titleText.setFormat(null, 20, FlxColor.fromRGB(255, 215, 0), FlxTextAlign.CENTER, FlxTextBorderStyle.OUTLINE, FlxColor.BLACK);
		titleText.setBorderStyle(FlxTextBorderStyle.OUTLINE, FlxColor.BLACK, 3);
		titleText.x = (FlxG.width - titleText.width) / 2;
		
		titleText.y = (FlxG.height - (titleText.height + titleGap + mystery1Text.height + mystery1Gap + mystery2Text.height + mystery2Gap + mystery3Text.height + mystery3Gap + backText.height)) / 2;
		mystery1Text.y = titleText.y + titleText.height + titleGap;
		mystery2Text.y = mystery1Text.y + mystery1Text.height + mystery1Gap;
		translationText1.y = mystery2Text.y + mystery2Text.height;
		mystery3Text.y = translationText1.y + translationText1.height + mystery2Gap;
		translationText2.y = mystery3Text.y + mystery3Text.height;
		mystery4Text.y = translationText2.y + translationText2.height + mystery3Gap;
		backText.y = translationText2.y + translationText2.height + mystery3Gap;
		
		character.watsonSprite.x = -character.watsonSprite.width;
		character.watsonSprite.y = FlxG.height - character.watsonSprite.height;
		
		add(settings.rainer4);
		add(settings.rainer3);
		add(character.watsonSprite);
		add(backText);
		add(mystery4Text);
		add(mystery1Text);
		add(mystery2Text);
		add(translationText1);
		add(mystery3Text);
		add(translationText2);
		add(mystery4Text);
		add(titleText);
		add(settings.rainer2);
		add(settings.rainer1);
		add(settings.floor);
		
		FlxG.sound.play(AssetPaths.rain04__ogg, settings.volume, true);
		
		FlxG.camera.fade(FlxColor.BLACK, 0.2, true);
		
		super.create();
		
	}

	override public function new(c:CharacterInfo, s:SettingsInfo) {
		
		super();
		
		character = c;
		
		settings = s;
		
	}
	
	private function switchToLoadingState():Void {
		
		remove(character.watsonSprite);
		remove(settings.floor);
		remove(settings.rainer1);
		remove(settings.rainer2);
		remove(settings.rainer3);
		remove(settings.rainer4);
		FlxG.switchState(new LoadingState(character, settings));
		
	}
	
	private function switchToMainMenuState():Void {
		
		remove(character.watsonSprite);
		remove(settings.floor);
		remove(settings.rainer1);
		remove(settings.rainer2);
		remove(settings.rainer3);
		remove(settings.rainer4);
		FlxG.switchState(new MainMenuState(character, settings));
		
	}
	
	override public function update(elapsed:Float):Void {
		
		if (settings.lightningEffects) {
		
			settings.flashCounter -= elapsed;
			if (settings.flashCounter < 0) {
				
				switch (settings.random.int(1, 6)) {
					
					case 1:
						FlxG.sound.play(AssetPaths.lightning01__ogg, settings.volume);
						
					case 2:
						FlxG.sound.play(AssetPaths.lightning02__ogg, settings.volume);
						
					case 3:
						FlxG.sound.play(AssetPaths.lightning03__ogg, settings.volume);
						
					case 4:
						FlxG.sound.play(AssetPaths.lightning04__ogg, settings.volume);
						
					case 5:
						FlxG.sound.play(AssetPaths.lightning05__ogg, settings.volume);
						
					case 6:
						FlxG.sound.play(AssetPaths.lightning06__ogg, settings.volume);
					
				}
				
				FlxG.camera.flash(FlxColor.WHITE, settings.random.float(0.1, 1.0));
				FlxG.camera.shake(0.001);
				settings.flashCounter = settings.random.float(1, 20);
				
			}
			
		}
		
		settings.characterCounter -= elapsed;
		if (settings.characterCounter < 0) {
			
			if (character.watsonSprite.animation.curAnim != null)
				character.watsonSprite.animation.finish();
			settings.characterCounter = settings.random.float(6, 60);
			
			if (settings.random.bool()) {
				
				character.watsonSprite.x = 0 - character.watsonSprite.width;
				character.watsonSprite.y = FlxG.height - character.watsonSprite.height;
				character.watsonSprite.animation.play("right");
				character.tween = FlxTween.tween(character.watsonSprite, {x: FlxG.width}, 5);
				
			} else {
				
				character.watsonSprite.x = FlxG.width;
				character.watsonSprite.y = FlxG.height - character.watsonSprite.height;
				character.watsonSprite.animation.play("left");
				character.tween = FlxTween.tween(character.watsonSprite, {x: (0 - character.watsonSprite.width)}, 5);
				
			}
			
		}
		
		if (settings.mysterySelectState == MysterySelectSubState.BACK_SELECTED) {
			
			backText.setBorderStyle(FlxTextBorderStyle.OUTLINE, bgColor.getLightened(), 3);
			mystery1Text.setBorderStyle(FlxTextBorderStyle.OUTLINE, FlxColor.BLACK, 3);
			mystery2Text.setBorderStyle(FlxTextBorderStyle.OUTLINE, FlxColor.BLACK, 3);
			mystery3Text.setBorderStyle(FlxTextBorderStyle.OUTLINE, FlxColor.BLACK, 3);
			mystery4Text.setBorderStyle(FlxTextBorderStyle.OUTLINE, FlxColor.BLACK, 3);
			translationText1.setBorderStyle(FlxTextBorderStyle.OUTLINE, FlxColor.BLACK, 1);
			translationText2.setBorderStyle(FlxTextBorderStyle.OUTLINE, FlxColor.BLACK, 1);
			
			if (FlxG.keys.anyJustPressed([UP, W])) {
				
				if (character.mystery4Unlocked)
					settings.mysterySelectState = MysterySelectSubState.MYSTERY_4_SELECTED;
				else
					settings.mysterySelectState = MysterySelectSubState.MYSTERY_3_SELECTED;
				
			} else if (FlxG.keys.anyJustPressed([DOWN, S])) {
				
				settings.mysterySelectState = MysterySelectSubState.MYSTERY_1_SELECTED;
				
			} else if (FlxG.keys.anyJustPressed([SPACE, ENTER])) {
				
				FlxG.camera.fade(FlxColor.BLACK, 0.2, false, switchToMainMenuState);
				
			}
			
		} else if (settings.mysterySelectState == MysterySelectSubState.MYSTERY_1_SELECTED) {
			
			backText.setBorderStyle(FlxTextBorderStyle.OUTLINE, FlxColor.BLACK, 3);
			mystery1Text.setBorderStyle(FlxTextBorderStyle.OUTLINE, bgColor.getLightened(), 3);
			mystery2Text.setBorderStyle(FlxTextBorderStyle.OUTLINE, FlxColor.BLACK, 3);
			mystery3Text.setBorderStyle(FlxTextBorderStyle.OUTLINE, FlxColor.BLACK, 3);
			mystery4Text.setBorderStyle(FlxTextBorderStyle.OUTLINE, FlxColor.BLACK, 3);
			translationText1.setBorderStyle(FlxTextBorderStyle.OUTLINE, FlxColor.BLACK, 1);
			translationText2.setBorderStyle(FlxTextBorderStyle.OUTLINE, FlxColor.BLACK, 1);
			
			if (FlxG.keys.anyJustPressed([UP, W])) {
				
				settings.mysterySelectState = MysterySelectSubState.BACK_SELECTED;
				
			} else if (FlxG.keys.anyJustPressed([DOWN, S])) {
				
				settings.mysterySelectState = MysterySelectSubState.MYSTERY_2_SELECTED;
				
			} else if (FlxG.keys.anyJustPressed([SPACE, ENTER])) {
				
				character.currentMystery = CharacterInfo.Mystery.MYSTERY1;
				character.location = "221B Baker Street";
				FlxG.camera.fade(FlxColor.BLACK, 0.2, false, switchToLoadingState);
				
			}
			
		} else if (settings.mysterySelectState == MysterySelectSubState.MYSTERY_2_SELECTED) {
			
			backText.setBorderStyle(FlxTextBorderStyle.OUTLINE, FlxColor.BLACK, 3);
			mystery1Text.setBorderStyle(FlxTextBorderStyle.OUTLINE, FlxColor.BLACK, 3);
			mystery2Text.setBorderStyle(FlxTextBorderStyle.OUTLINE, bgColor.getLightened(), 3);
			mystery3Text.setBorderStyle(FlxTextBorderStyle.OUTLINE, FlxColor.BLACK, 3);
			mystery4Text.setBorderStyle(FlxTextBorderStyle.OUTLINE, FlxColor.BLACK, 3);
			translationText1.setBorderStyle(FlxTextBorderStyle.OUTLINE, bgColor.getLightened(), 1);
			translationText2.setBorderStyle(FlxTextBorderStyle.OUTLINE, FlxColor.BLACK, 1);
			
			if (FlxG.keys.anyJustPressed([UP, W])) {
				
				settings.mysterySelectState = MysterySelectSubState.MYSTERY_1_SELECTED;
				
			} else if (FlxG.keys.anyJustPressed([DOWN, S])) {
				
				settings.mysterySelectState = MysterySelectSubState.MYSTERY_3_SELECTED;
				
			} else if (FlxG.keys.anyJustPressed([SPACE, ENTER])) {
				
				character.currentMystery = CharacterInfo.Mystery.MYSTERY2;
				character.location = "Mystery Select State";
				FlxG.camera.fade(FlxColor.BLACK, 0.2, false, switchToLoadingState);
				
			}
			
		} else if (settings.mysterySelectState == MysterySelectSubState.MYSTERY_3_SELECTED) {
			
			backText.setBorderStyle(FlxTextBorderStyle.OUTLINE, FlxColor.BLACK, 3);
			mystery1Text.setBorderStyle(FlxTextBorderStyle.OUTLINE, FlxColor.BLACK, 3);
			mystery2Text.setBorderStyle(FlxTextBorderStyle.OUTLINE, FlxColor.BLACK, 3);
			mystery3Text.setBorderStyle(FlxTextBorderStyle.OUTLINE, bgColor.getLightened(), 3);
			mystery4Text.setBorderStyle(FlxTextBorderStyle.OUTLINE, FlxColor.BLACK, 3);
			translationText1.setBorderStyle(FlxTextBorderStyle.OUTLINE, FlxColor.BLACK, 1);
			translationText2.setBorderStyle(FlxTextBorderStyle.OUTLINE, bgColor.getLightened(), 1);
			
			if (FlxG.keys.anyJustPressed([UP, W])) {
				
				settings.mysterySelectState = MysterySelectSubState.MYSTERY_2_SELECTED;
				
			} else if (FlxG.keys.anyJustPressed([DOWN, S])) {
				
				if (character.mystery4Unlocked)
					settings.mysterySelectState = MysterySelectSubState.MYSTERY_4_SELECTED;
				else
					settings.mysterySelectState = MysterySelectSubState.BACK_SELECTED;
				
			} else if (FlxG.keys.anyJustPressed([SPACE, ENTER])) {
				
				character.currentMystery = CharacterInfo.Mystery.MYSTERY3;
				character.location = "Mystery Select State";
				FlxG.camera.fade(FlxColor.BLACK, 0.2, false, switchToLoadingState);
				
			}
			
		} else if (settings.mysterySelectState == MysterySelectSubState.MYSTERY_4_SELECTED) {
			
			backText.setBorderStyle(FlxTextBorderStyle.OUTLINE, FlxColor.BLACK, 3);
			mystery1Text.setBorderStyle(FlxTextBorderStyle.OUTLINE, FlxColor.BLACK, 3);
			mystery2Text.setBorderStyle(FlxTextBorderStyle.OUTLINE, FlxColor.BLACK, 3);
			mystery3Text.setBorderStyle(FlxTextBorderStyle.OUTLINE, FlxColor.BLACK, 3);
			mystery4Text.setBorderStyle(FlxTextBorderStyle.OUTLINE, bgColor.getLightened(), 3);
			translationText1.setBorderStyle(FlxTextBorderStyle.OUTLINE, FlxColor.BLACK, 1);
			translationText2.setBorderStyle(FlxTextBorderStyle.OUTLINE, FlxColor.BLACK, 1);
			
			if (FlxG.keys.anyJustPressed([UP, W])) {
				
				settings.mysterySelectState = MysterySelectSubState.MYSTERY_3_SELECTED;
				
			} else if (FlxG.keys.anyJustPressed([DOWN, S])) {
				
				settings.mysterySelectState = MysterySelectSubState.BACK_SELECTED;
				
			} else if (FlxG.keys.anyJustPressed([SPACE, ENTER])) {
				
				character.currentMystery = CharacterInfo.Mystery.MYSTYER4;
				character.location = "Dark World";
				FlxG.camera.fade(FlxColor.BLACK, 0.2, false, switchToLoadingState);
				
			}
			
		}
		
		if (FlxG.keys.anyJustPressed([ESCAPE, BACKSPACE])) {
			
			FlxG.camera.fade(FlxColor.BLACK, 0.2, false, switchToMainMenuState);
			
		} else if (FlxG.keys.anyJustPressed([END])) {
			
			character.mystery4Unlocked = true;
			FlxG.sound.play(AssetPaths.positive02__ogg, settings.volume, false);
			FlxTween.tween(backText, {y: mystery4Text.y + mystery4Text.height + mystery4Gap}, 1);
			FlxTween.tween(mystery4Text, {alpha: 1}, 1);
			FlxG.camera.shake(0.001, 1);
			
		}
		
		FlxG.collide(settings.rainer1, settings.floor);
		FlxG.collide(settings.rainer2, settings.floor);
		FlxG.collide(settings.rainer3, settings.floor);
		FlxG.collide(settings.rainer4, settings.floor);
		
		super.update(elapsed);
		
	}
	
}
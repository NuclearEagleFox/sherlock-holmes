package;

import flixel.text.FlxText;
import flixel.tweens.FlxTween;
import flixel.util.FlxColor;
import flixel.FlxG;
import flixel.FlxState;

enum CreditsSubState {
	
	CREATED_BY;
	QUOTE;
	ATTRIBUTION;
	
}

class CreditsState extends FlxState {
	
	var character:CharacterInfo;
	
	var attributionText:FlxText;
	var createdByText:FlxText;
	var quoteText:FlxText;
	
	var currentTween:FlxTween;
	
	var settings:SettingsInfo;
	
	override public function create():Void {
		
		character.watsonSprite.x = -character.watsonSprite.width;
		character.watsonSprite.y = FlxG.height - character.watsonSprite.height;
		
		createdByText = new FlxText(0, 0, "Created by NuclearEagleFox");
		createdByText.setFormat(null, 30, FlxColor.fromRGB(255, 215, 0), FlxTextAlign.CENTER);
		createdByText.setBorderStyle(FlxTextBorderStyle.OUTLINE, bgColor.getLightened(), 3);
		createdByText.alpha = 0;
		createdByText.x = (FlxG.width - createdByText.width) / 2;
		createdByText.y = (FlxG.height - createdByText.height) / 2;
		
		quoteText = new FlxText(0, 0, "\"I'm slowly realizing I could've made this game in RPG Maker.\"");
		quoteText.setFormat(null, 15, FlxColor.fromRGB(255, 215, 0), FlxTextAlign.CENTER);
		quoteText.setBorderStyle(FlxTextBorderStyle.OUTLINE, bgColor.getLightened(), 3);
		quoteText.alpha = 0;
		quoteText.x = (FlxG.width - quoteText.width) / 2;
		quoteText.y = createdByText.y + createdByText.height;
		
		attributionText = new FlxText(0, 0, FlxG.width - 50, "Assets Pulled From:\nFreesound, Humble Game Making Bundle");
		attributionText.setFormat(null, 15, FlxColor.fromRGB(255, 215, 0), FlxTextAlign.CENTER);
		attributionText.setBorderStyle(FlxTextBorderStyle.OUTLINE, bgColor.getLightened(), 3);
		attributionText.alpha = 0;
		attributionText.x = (FlxG.width - attributionText.width) / 2;
		attributionText.y = quoteText.y + quoteText.height + 20;
		
		add(settings.rainer4);
		add(settings.rainer3);
		add(createdByText);
		add(quoteText);
		add(attributionText);
		add(character.watsonSprite);
		add(settings.rainer2);
		add(settings.rainer1);
		add(settings.floor);
		
		currentTween = FlxTween.tween(createdByText, {alpha: 1}, 1, {startDelay: 1});
		
		FlxG.sound.play(AssetPaths.rain04__ogg, settings.volume, true);
		FlxG.camera.fade(FlxColor.BLACK, 0.2, true);
		
		super.create();
		
	}

	public function new(c:CharacterInfo, s:SettingsInfo) {
		
		super();
		
		character = c;
		
		settings = s;
		
	}
	
	private function switchToLoadingState():Void {
		
		clear();
		FlxG.switchState(new LoadingState(character, settings));
		
	}
	
	override public function update(elapsed:Float):Void {
		
		if (settings.lightningEffects) {
			
			settings.flashCounter -= elapsed;
			if (settings.flashCounter < 0) {
				
				switch (settings.random.int(1, 6)) {
					
					case 1:
						FlxG.sound.play(AssetPaths.lightning01__ogg, settings.volume);
						
					case 2:
						FlxG.sound.play(AssetPaths.lightning02__ogg, settings.volume);
						
					case 3:
						FlxG.sound.play(AssetPaths.lightning03__ogg, settings.volume);
						
					case 4:
						FlxG.sound.play(AssetPaths.lightning04__ogg, settings.volume);
						
					case 5:
						FlxG.sound.play(AssetPaths.lightning05__ogg, settings.volume);
						
					case 6:
						FlxG.sound.play(AssetPaths.lightning06__ogg, settings.volume);
					
				}
				
				FlxG.camera.flash(FlxColor.WHITE, settings.random.float(0.1, 1.0));
				FlxG.camera.shake(0.001);
				settings.flashCounter = settings.random.float(1, 20);
				
			}
			
		}
		
		settings.characterCounter -= elapsed;
		if (settings.characterCounter < 0) {
			
			if (character.watsonSprite.animation.curAnim != null)
				character.watsonSprite.animation.finish();
			settings.characterCounter = settings.random.float(6, 60);
			
			if (settings.random.bool()) {
				
				character.watsonSprite.x = 0 - character.watsonSprite.width;
				character.watsonSprite.y = FlxG.height - character.watsonSprite.height;
				character.watsonSprite.animation.play("right");
				character.tween = FlxTween.tween(character.watsonSprite, {x: FlxG.width}, 5);
				
			} else {
				
				character.watsonSprite.x = FlxG.width;
				character.watsonSprite.y = FlxG.height - character.watsonSprite.height;
				character.watsonSprite.animation.play("left");
				character.tween = FlxTween.tween(character.watsonSprite, {x: (0 - character.watsonSprite.width)}, 5);
				
			}
			
		}
		
		if (settings.creditsState == CreditsSubState.CREATED_BY) {
			
			if (currentTween.finished) {
				
				currentTween = FlxTween.tween(quoteText, {alpha: 1}, 1, {startDelay: 1});
				settings.creditsState = CreditsSubState.QUOTE;
				
			}
			
		} else if (settings.creditsState == CreditsSubState.QUOTE) {
			
			if (currentTween.finished) {
				
				currentTween = FlxTween.tween(attributionText, {alpha: 1}, 1, {startDelay: 1});
				settings.creditsState = CreditsSubState.ATTRIBUTION;
				
			}
			
		}
		
		if (FlxG.keys.anyJustPressed([SPACE, ENTER, ESCAPE])) {
			
			character.location = "Main Menu State";
			FlxG.camera.fade(FlxColor.BLACK, 0.2, false, switchToLoadingState);
			
		}
		
		FlxG.collide(settings.rainer1, settings.floor);
		FlxG.collide(settings.rainer2, settings.floor);
		FlxG.collide(settings.rainer3, settings.floor);
		FlxG.collide(settings.rainer4, settings.floor);
		
		super.update(elapsed);
		
	}
	
}
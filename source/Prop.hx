package;

import flixel.FlxG;
import flixel.FlxSprite;
import flixel.system.FlxAssets.FlxGraphicAsset;
import flixel.util.FlxColor;
import sys.FileSystem;
import sys.io.FileInput;

class Prop extends FlxSprite {
	
	private var lines:Array<String>;
	public static var masterList:Array<String> = ["bed", "chair_down", "chair_up", "chest", "crate", "curtain", "curtain_top", "daniel", "dark_chair_down", "dark_chair_up", "dark_curtain", "dark_skelly", "desk", "door", "doors", "holmes", "long_bookcase", "maze_crate", "messy_bed", "mutant_holmes", "musgrave", "other_curtain", "picture", "shed", "shed_top", "shed_trigger", "skelly", "tall_bookcase", "tree", "tree_top", "watson"];
	
	public var layer(default, null):Int;
	
	private var lineIndex:Int;
	
	private var animatedMap:Map<String, Bool>           = ["bed"=>false,               "chair_down"=>false,                      "chair_up"=>false,                    "chest"=>false,                 "crate"=>false,                 "curtain"=>false,                   "daniel"=>true,                   "dark_chair_down"=>false,                      "dark_chair_up"=>false,                    "dark_curtain"=>false,                   "dark_skelly"=>false,                  "curtain_top"=>false,                       "desk"=>false,                "door"=>false,                "doors"=>false,                 "holmes"=>true,                   "long_bookcase"=>false,                     "maze_crate"=>false,                 "messy_bed"=>false,                     "mutant_holmes"=>false,                  "musgrave"=>true,                     "other_curtain"=>false,                   "picture"=>false,                   "shed"=>false,                "shed_top"=>false,                    "shed_trigger"=>false,                "skelly"=>false,                  "tall_bookcase"=>false,                     "tree"=>false,                "tree_top"=>false,                    "watson"=>true];
	private var graphicMap:Map<String, FlxGraphicAsset> = ["bed"=>AssetPaths.bed__png, "chair_down"=>AssetPaths.chair_down__png, "chair_up"=>AssetPaths.chair_up__png, "chest"=>AssetPaths.chest__png, "crate"=>AssetPaths.crate__png, "curtain"=>AssetPaths.curtain__png, "daniel"=>AssetPaths.daniel__png, "dark_chair_down"=>AssetPaths.chair_down__png, "dark_chair_up"=>AssetPaths.chair_up__png, "dark_curtain"=>AssetPaths.curtain__png, "dark_skelly"=>AssetPaths.skelly__png, "curtain_top"=>AssetPaths.curtain_top__png, "desk"=>AssetPaths.desk__png, "door"=>AssetPaths.door__png, "doors"=>AssetPaths.doors__png, "holmes"=>AssetPaths.holmes__png, "long_bookcase"=>AssetPaths.bookcase0__png, "maze_crate"=>AssetPaths.crate__png, "messy_bed"=>AssetPaths.messy_bed__png, "mutant_holmes"=>AssetPaths.holmes__png, "musgrave"=>AssetPaths.musgrave__png, "other_curtain"=>AssetPaths.curtain__png, "picture"=>AssetPaths.picture__png, "shed"=>AssetPaths.shed__png, "shed_top"=>AssetPaths.shed_top__png, "shed_trigger"=>AssetPaths.shed__png, "skelly"=>AssetPaths.skelly__png, "tall_bookcase"=>AssetPaths.bookcase1__png, "tree"=>AssetPaths.tree__png, "tree_top"=>AssetPaths.tree_top__png, "watson"=>AssetPaths.watson__png];
	private var heightMap:Map<String, Float>            = ["bed"=>(36 * 2),            "chair_down"=>36,                         "chair_up"=>36,                       "chest"=>36,                    "crate"=>36,                    "curtain"=>36,                      "daniel"=>36,                     "dark_chair_down"=>36,                         "dark_chair_up"=>36,                       "dark_curtain"=>36,                      "dark_skelly"=>36,                     "curtain_top"=>36,                          "desk"=>36,                   "door"=>(36 * 2),             "doors"=>61,                    "holmes"=>36,                     "long_bookcase"=>36,                        "maze_crate"=>36,                    "messy_bed"=>(36 * 2),                  "mutant_holmes"=>36,                     "musgrave"=>36,                       "other_curtain"=>36,                      "picture"=>36,                      "shed"=>(36 * 2),             "shed_top"=>36,                       "shed_trigger"=>36,                   "skelly"=>36,                     "tall_bookcase"=>(36 * 2),                  "tree"=>36,                   "tree_top"=>36,                       "watson"=>36];
	private var layerMap:Map<String, Int>               = ["bed"=>1,                   "chair_down"=>1,                          "chair_up"=>1,                        "chest"=>1,                     "crate"=>2,                     "curtain"=>1,                       "daniel"=>2,                      "dark_chair_down"=>1,                          "dark_chair_up"=>1,                        "dark_curtain"=>1,                       "dark_skelly"=>1,                      "curtain_top"=>3,                           "desk"=>1,                    "door"=>1,                    "doors"=>2,                     "holmes"=>1,                      "long_bookcase"=>1,                         "maze_crate"=>1,                     "messy_bed"=>1,                         "mutant_holmes"=>1,                      "musgrave"=>2,                        "other_curtain"=>1,                       "picture"=>1,                       "shed"=>1,                    "shed_top"=>3,                        "shed_trigger"=>0,                    "skelly"=>1,                      "tall_bookcase"=>1,                         "tree"=>1,                    "tree_top"=>3,                        "watson"=>0];
	private var nameMap:Map<String, String>             = ["bed" => "Bed",             "chair_down" => "Wife Chair",             "chair_up" => "Husband Chair",        "chest"=>"Chest",               "crate"=>"Crate",               "curtain" => "Curtain",             "daniel"=>"Daniel",               "dark_chair_down"=>"Wife Chair",               "dark_chair_up"=>"Husband Chair",          "dark_curtain"=>"Special Curtain",       "dark_skelly"=>"Skellybro",            "curtain_top"=>"Curtain Top",               "desk" => "Desk",             "door" => "Door",             "doors"=> "Manor Doors",        "holmes" => "Holmes",             "long_bookcase" => "Bookcase",              "maze_crate"=>"Crate",               "messy_bed" => "Bed",                   "mutant_holmes"=>"Holmes",               "musgrave" => "Musgrave",             "other_curtain" => "Special Curtain",     "picture" => "Picture",             "shed"=>"Shed",               "shed_top"=>"Shed Top",               "shed_trigger"=>"Shed",               "skelly"=>"Skeleton",             "tall_bookcase" => "Shy Bookcase",          "tree"=>"Oak Tree",           "tree_top"=>"Tree Top",               "watson"=>"Watson"];
	private var widthMap:Map<String, Float>             = ["bed"=>36,                  "chair_down"=>36,                         "chair_up"=>36,                       "chest"=>36,                    "crate"=>36,                    "curtain"=>36,                      "daniel"=>36,                     "dark_chair_down"=>36,                         "dark_chair_up"=>36,                       "dark_curtain"=>36,                      "dark_skelly"=>36,                     "curtain_top"=>36,                          "desk"=>(36 * 3),             "door"=>36,                   "doors"=>72,                    "holmes"=>36,                     "long_bookcase"=>(36 * 2),                  "maze_crate"=>36,                    "messy_bed"=>36,                        "mutant_holmes"=>36,                     "musgrave"=>36,                       "other_curtain"=>36,                      "picture"=>36,                      "shed"=>(36 * 5),             "shed_top"=>(36 * 5),                 "shed_trigger"=>36,                   "skelly"=>36,                     "tall_bookcase"=>36,                        "tree"=>36,                   "tree_top"=>(36 * 3),                 "watson"=>36];
	private var frameHeightMap:Map<String, Int> = ["holmes" => 52, "daniel"=>52, "musgrave" => 52, "watson"=>52];
	private var frameWidthMap:Map<String, Int>  = ["holmes" => 36, "daniel"=>36, "musgrave" => 36, "watson"=>36];
	
	public var location(default, null):String;
	public var name(default, null):String;
	public var prettyName(default, null):String;
	
	public function getNextLine():String {
		
		if (lineIndex < lines.length) {
			
			var line:String = lines[lineIndex];
			lineIndex++;
			return line;
			
		} else {
			
			lineIndex = 0;
			return null;
			
		}
		
	}

	public function new(l:String, n:String, ?X:Float = 0, ?Y:Float = 0, ?SimpleGraphic:FlxGraphicAsset) {
		
		super(X, Y, SimpleGraphic);
		
		location = l;
		
		name = n;
		
		lines = new Array<String>();
		var fin:FileInput = null;
		if (FileSystem.exists("./assets/data/" + name + ".txt"))
			fin = sys.io.File.read("./assets/data/" + name + ".txt", false);
		else
			FlxG.log.add("Missing: " + "./assets/data/" + name + ".txt");
		while (fin != null && !fin.eof()) {
			
			var newLine:String = fin.readLine();
			lines.push(newLine);
			
		}
		
		layer = layerMap.get(name);
		
		lineIndex = 0;
		
		if (animatedMap.get(name)) {
			
			loadGraphic(graphicMap.get(name), animatedMap.get(name), frameWidthMap.get(name), frameHeightMap.get(name));
			animation.frameIndex = 1;
			
		} else {
			
			if (graphicMap.get(name) == null)
				makeGraphic(36, 36, FlxColor.TRANSPARENT);
			else
				loadGraphic(graphicMap.get(name));
			
		}
		
		height = heightMap.get(name);
		
		width = widthMap.get(name);
		
		if (name == "tree")
			offset.set(36, frameHeight - height);
		else
			offset.set(0, frameHeight - height);
		
		immovable = true;
		
		moves = false;
		
		prettyName = nameMap.get(name);
		
	}
	
	public function resetLines():Void {
		
		lineIndex = 0;
		
	}
	
}
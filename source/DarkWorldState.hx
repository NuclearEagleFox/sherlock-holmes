package;

import flixel.FlxCamera.FlxCameraFollowStyle;
import flash.system.System;
import flixel.FlxG;
import flixel.FlxObject;
import flixel.FlxSprite;
import flixel.FlxState;
import flixel.addons.editors.ogmo.FlxOgmoLoader;
import flixel.group.FlxGroup;
import flixel.math.FlxMath;
import flixel.math.FlxPoint;
import flixel.math.FlxRect;
import flixel.text.FlxText;
import flixel.tile.FlxTilemap;
import flixel.tweens.FlxTween;
import flixel.util.FlxColor;

class DarkWorldState extends FlxState {
	
	var locationTexts:Array<FlxText>;
	
	var character:CharacterInfo;
	
	var exitTextYOffset:Float;
	var locationsTextYOffset:Float;
	var mainMenuTextYOffset:Float;
	var resumeTextYOffset:Float;
	var speechRectYOffset:Float = 5;
	
	var layer0:FlxGroup;
	var layer1:FlxGroup;
	var layer2:FlxGroup;
	var layer3:FlxGroup;
	
	var mapLoader:FlxOgmoLoader;
	
	var speechRect:FlxRect;
	
	var locationTitleText:FlxText;
	var exitText:FlxText;
	var locationsText:FlxText;
	var mainMenuText:FlxText;
	var resumeText:FlxText;
	
	var map:FlxTilemap;
	
	var locationsMenuSelectedIndex:Int;
	
	var danielProp:Prop;
	var talkingProp:Prop;
	
	var settings:SettingsInfo;
	
	override public function create():Void {
		
		FlxG.log.add("Dark World State");
		
		locationTexts = new Array<FlxText>();
		
		exitText = new FlxText(0, 0, "Exit");
		exitText.alpha = 0;
		exitText.setFormat(null, 20, FlxColor.fromRGB(255, 215, 0), FlxTextAlign.CENTER, FlxTextBorderStyle.OUTLINE, FlxColor.BLACK);
		exitText.setBorderStyle(FlxTextBorderStyle.OUTLINE, FlxColor.BLACK, 3);
		exitText.x = FlxG.camera.scroll.x + ((FlxG.width - exitText.width) / 2);
		exitTextYOffset = FlxG.camera.scroll.y - exitText.height;
		exitText.y = exitTextYOffset;
		
		locationsText = new FlxText(0, 0, "Locations");
		locationsText.alpha = 0;
		locationsText.setFormat(null, 20, FlxColor.fromRGB(255, 215, 0), FlxTextAlign.CENTER, FlxTextBorderStyle.OUTLINE, FlxColor.BLACK);
		locationsText.setBorderStyle(FlxTextBorderStyle.OUTLINE, FlxColor.BLACK, 3);
		locationsText.x = FlxG.camera.scroll.x + ((FlxG.width - locationsText.width) / 2);
		locationsTextYOffset = FlxG.camera.scroll.y - locationsText.height;
		locationsText.y = locationsTextYOffset;
		
		mainMenuText = new FlxText(0, 0, "Main Menu");
		mainMenuText.alpha = 0;
		mainMenuText.setFormat(null, 20, FlxColor.fromRGB(255, 215, 0), FlxTextAlign.CENTER, FlxTextBorderStyle.OUTLINE, FlxColor.BLACK);
		mainMenuText.setBorderStyle(FlxTextBorderStyle.OUTLINE, FlxColor.BLACK, 3);
		mainMenuText.x = FlxG.camera.scroll.x + ((FlxG.width - mainMenuText.width) / 2);
		mainMenuTextYOffset = FlxG.camera.scroll.y - mainMenuText.height;
		mainMenuText.y = mainMenuTextYOffset;
		
		resumeText = new FlxText(0, 0, "Resume");
		resumeText.alpha = 0;
		resumeText.setFormat(null, 20, FlxColor.fromRGB(255, 215, 0), FlxTextAlign.CENTER, FlxTextBorderStyle.OUTLINE, FlxColor.BLACK);
		resumeText.setBorderStyle(FlxTextBorderStyle.OUTLINE, FlxColor.BLACK, 3);
		resumeText.x = FlxG.camera.scroll.x + ((FlxG.width - resumeText.width) / 2);
		resumeTextYOffset = FlxG.camera.scroll.y - resumeText.height;
		resumeText.y = resumeTextYOffset;
		
		locationTitleText = new FlxText(0, 0, character.location);
		locationTitleText.setFormat(null, 30, FlxColor.fromRGB(255, 215, 0), FlxTextAlign.CENTER, FlxTextBorderStyle.OUTLINE, FlxColor.BLACK);
		locationTitleText.setBorderStyle(FlxTextBorderStyle.OUTLINE, FlxColor.BLACK, 3);
		locationTitleText.alpha = 0.0;
		locationTitleText.x = FlxG.camera.scroll.x + ((FlxG.width - locationTitleText.width) / 2);
		locationTitleText.y = FlxG.camera.scroll.y + ((FlxG.height - locationTitleText.height) / 2);
		FlxTween.tween(locationTitleText, {alpha: 1.0}, 0.5, {onComplete: fadeText, startDelay: 1});
		
		layer0 = new FlxGroup();
		layer1 = new FlxGroup();
		layer2 = new FlxGroup();
		layer3 = new FlxGroup();
		
		mapLoader = new FlxOgmoLoader(AssetPaths.Dark_World__oel);
		map = mapLoader.loadTilemap(AssetPaths.tiles2__png, 36, 36, "walls");
		
		for (i in 0...70) {
			
			map.setTileProperties(i, FlxObject.NONE);
			
		}
		
		speechRect = new FlxRect(0, 0, 0, 0);
		talkingProp = new Prop(character.location, "bed", 0, speechRect.y);
		if (talkingProp.frameHeight > talkingProp.frameWidth) {
			
			talkingProp.scale.set((character.bubbleSprite.height - character.nameText.height) / talkingProp.frameHeight, (character.bubbleSprite.height - character.nameText.height) / talkingProp.frameHeight);
			talkingProp.updateHitbox();
			
		} else {
			
			talkingProp.scale.set((FlxG.width - character.bubbleSprite.width - 10) / talkingProp.frameWidth, (FlxG.width - character.bubbleSprite.width - 10) / talkingProp.frameWidth);
			talkingProp.updateHitbox();
			
		}
		
		speechRect.height = (character.bubbleSprite.height >= (talkingProp.height + character.nameText.height)) ? character.bubbleSprite.height : (talkingProp.height + character.nameText.height);
		speechRect.width = character.bubbleSprite.width + ((talkingProp.width >= character.nameText.width) ? talkingProp.width : character.nameText.width);
		speechRect.x = FlxG.camera.scroll.x + ((FlxG.width - speechRect.width) / 2);
		speechRect.y = FlxG.camera.scroll.y + FlxG.height;
		
		character.bubbleSprite.x = speechRect.x + speechRect.width - character.bubbleSprite.width;
		character.bubbleSprite.y = speechRect.y + ((speechRect.height - character.bubbleSprite.height) / 2);
		
		character.speechText.x = character.bubbleSprite.x + 32;
		character.speechText.y = character.bubbleSprite.y + ((character.bubbleSprite.height - character.speechText.height) / 2);
		
		talkingProp.x = speechRect.x + ((((talkingProp.width >= character.nameText.width) ? talkingProp.width : character.nameText.width) - talkingProp.width) / 2);
		talkingProp.y = speechRect.y + ((speechRect.height - talkingProp.height - character.nameText.height) / 2);
		
		character.nameText.x = speechRect.x + ((((talkingProp.width >= character.nameText.width) ? talkingProp.width : character.nameText.width) - character.nameText.width) / 2);
		character.nameText.y = talkingProp.y + talkingProp.height;
		
		character.alertSprite.x = character.watsonSprite.x;
		character.alertSprite.y = character.watsonSprite.y - 52;
		
		mapLoader.loadEntities(placeEntities, "entities");
		
		add(map);
		add(layer0);
		add(layer1);
		add(character.watsonSprite);
		add(layer2);
		add(layer3);
		add(locationTitleText);
		add(character.bubbleSprite);
		add(character.speechText);
		add(character.nameText);
		add(talkingProp);
		add(resumeText);
		add(locationsText);
		add(mainMenuText);
		add(exitText);
		add(character.alertSprite);
		
		FlxG.camera.follow(character.watsonSprite, FlxCameraFollowStyle.TOPDOWN);
		FlxG.camera.fade(FlxColor.BLACK, 0.2, true);
		
		super.create();
		
	}
	
	private function fadeText(t:FlxTween):Void {
		
		FlxTween.tween(locationTitleText, {alpha: 0}, 0.5, {startDelay: 3});
		
	}
	
	private function getSpriteAt(point:FlxPoint, group:FlxGroup):Prop {
		
		var sprites:Array<FlxSprite> = new Array<FlxSprite>();
		
		for (s in group) {
			
			var sprite:Prop = cast(s, Prop);
			
			if (point.x >= sprite.x && point.x <= (sprite.x + sprite.width) && point.y >= sprite.y && point.y <= (sprite.y + sprite.height)) {
				
				return sprite;
				
			}
			
		}
		
		return null;
		
	}

	public function new(c:CharacterInfo, s:SettingsInfo) {
		
		super();
		
		character = c;
		
		settings = s;
		
	}
	
	private function placeEntities(entityName:String, entityData:Xml):Void {
		
		var x:Int = Std.parseInt(entityData.get("x"));
		var y:Int = Std.parseInt(entityData.get("y"));
		
		if (entityName == "player") {
			
			character.watsonSprite.x = x;
			character.watsonSprite.y = y;
			
		} else if (entityName == "daniel") {
			
			danielProp = new Prop(character.location, entityName, x, y);
			switch (danielProp.layer) {
				
				case 0:
					layer0.add(danielProp);
					
				case 1:
					layer1.add(danielProp);
					
				case 2:
					layer2.add(danielProp);
					
				case 3:
					layer3.add(danielProp);
				
			}
			
		} else if (Prop.masterList.indexOf(entityName) < 0) {
			
			FlxG.log.add("Missing Entity: " + entityName);
			
		} else {
			
			var prop:Prop = new Prop(character.location, entityName, x, y);
			switch (prop.layer) {
				
				case 0:
					layer0.add(prop);
					
				case 1:
					layer1.add(prop);
					
				case 2:
					layer2.add(prop);
					
				case 3:
					layer3.add(prop);
				
			}
			
		}
		
	}
	
	private function switchToExit():Void {
		
		System.exit(0);
		
	}
	
	private function switchToLoadingState():Void {
		
		clear();
		FlxG.switchState(new LoadingState(character, settings));
		
	}
	
	override public function update(elapsed:Float):Void	{
		
		FlxG.worldBounds.set(FlxG.camera.scroll.x, FlxG.camera.scroll.y, FlxG.width, FlxG.height);
		
		exitText.x = FlxG.camera.scroll.x + ((FlxG.width - exitText.width) / 2);
		locationsText.x = FlxG.camera.scroll.x + ((FlxG.width - locationsText.width) / 2);
		mainMenuText.x = FlxG.camera.scroll.x + ((FlxG.width - mainMenuText.width) / 2);
		resumeText.x = FlxG.camera.scroll.x + ((FlxG.width - resumeText.width) / 2);
		
		exitText.y = exitTextYOffset;
		locationsText.y = locationsTextYOffset;
		mainMenuText.y = mainMenuTextYOffset;
		resumeText.y = resumeTextYOffset;
		
		locationTitleText.x = FlxG.camera.scroll.x + ((FlxG.width - locationTitleText.width) / 2);
		locationTitleText.y = FlxG.camera.scroll.y + ((FlxG.height - locationTitleText.height) / 2);
		
		character.alertSprite.x = character.watsonSprite.x;
		character.alertSprite.y = character.watsonSprite.y - 52;
		
		speechRect.x = FlxG.camera.scroll.x + ((FlxG.width - speechRect.width) / 2);
		speechRect.y = FlxG.camera.scroll.y + FlxG.height + speechRectYOffset;
		
		character.bubbleSprite.x = speechRect.x + speechRect.width - character.bubbleSprite.width;
		character.bubbleSprite.y = speechRect.y + ((speechRect.height - character.bubbleSprite.height) / 2);
		
		talkingProp.x = speechRect.x + ((((talkingProp.width >= character.nameText.width) ? talkingProp.width : character.nameText.width) - talkingProp.width) / 2);
		talkingProp.y = speechRect.y + ((speechRect.height - talkingProp.height - character.nameText.height) / 2);
		
		character.nameText.x = speechRect.x + ((((talkingProp.width >= character.nameText.width) ? talkingProp.width : character.nameText.width) - character.nameText.width) / 2);
		character.nameText.y = talkingProp.y + talkingProp.height;
		
		character.speechText.x = character.bubbleSprite.x + 32;
		character.speechText.y = character.bubbleSprite.y + ((character.bubbleSprite.height - character.speechText.height) / 2);
		
		if (character.state == CharacterInfo.CharacterState.INTERACTING) {
			
			if (character.interactingWith != null) {
				
				if (FlxG.keys.anyJustPressed([ENTER, SPACE])) {
					
					var line:String = character.interactingWith.getNextLine();
					
					if (line != null) {
						
						character.speechText.text = line;
						
					} else {
						
						if (character.interactingWith.name == "daniel") {
							
							if (character.unlockedLocations.indexOf("Credits") < 0)
								character.unlockedLocations.push("Credits");
							character.alertSprite.animation.play("alert");
							FlxG.sound.play(AssetPaths.positive02__ogg, settings.volume);
							
						}
						
						FlxTween.tween(this, {speechRectYOffset: 5}, 0.2);
						character.state = CharacterInfo.CharacterState.STATIONARY;
						
					}
					
				} else if (FlxG.keys.anyJustPressed([ESCAPE, BACKSPACE])) {
					
					character.interactingWith.resetLines();
					FlxTween.tween(this, {speechRectYOffset: 5}, 0.2);
					character.state = CharacterInfo.CharacterState.STATIONARY;
					
				}
				
			} else {
				
				character.state = CharacterInfo.CharacterState.STATIONARY;
				
			}
			
		} else if (character.state == CharacterInfo.CharacterState.MENU) {
			
			if (settings.gameMenuState == CharacterInfo.GameMenuState.EXIT_SELECTED) {
				
				exitText.setBorderStyle(FlxTextBorderStyle.OUTLINE, bgColor.getLightened(), 3);
				locationsText.setBorderStyle(FlxTextBorderStyle.OUTLINE, FlxColor.BLACK, 3);
				mainMenuText.setBorderStyle(FlxTextBorderStyle.OUTLINE, FlxColor.BLACK, 3);
				resumeText.setBorderStyle(FlxTextBorderStyle.OUTLINE, FlxColor.BLACK, 3);
				
				if (FlxG.keys.anyJustPressed([UP, W])) {
					
					settings.gameMenuState = CharacterInfo.GameMenuState.MAIN_MENU_SELECTED;
					
				} else if (FlxG.keys.anyJustPressed([DOWN, S])) {
					
					settings.gameMenuState = CharacterInfo.GameMenuState.RESUME_SELECTED;
					
				} else if (FlxG.keys.anyJustPressed([SPACE, ENTER])) {
					
					FlxG.camera.fade(FlxColor.BLACK, 0.2, false, switchToExit);
					
				} else if (FlxG.keys.anyJustPressed([ESCAPE, BACKSPACE])) {
					
					FlxTween.tween(exitText, {alpha: 0}, settings.menuSpeed);
					FlxTween.tween(this, {exitTextYOffset: FlxG.camera.scroll.y - exitText.height}, settings.menuSpeed);
					FlxTween.tween(locationsText, {alpha: 0}, settings.menuSpeed);
					FlxTween.tween(this, {locationsTextYOffset: FlxG.camera.scroll.y - locationsText.height}, settings.menuSpeed);
					FlxTween.tween(mainMenuText, {alpha: 0}, settings.menuSpeed);
					FlxTween.tween(this, {mainMenuTextYOffset: FlxG.camera.scroll.y - mainMenuText.height}, settings.menuSpeed);
					FlxTween.tween(resumeText, {alpha: 0}, settings.menuSpeed);
					FlxTween.tween(this, {resumeTextYOffset: FlxG.camera.scroll.y - resumeText.height}, settings.menuSpeed);
					character.state = CharacterInfo.CharacterState.STATIONARY;
					
				}
				
			} else if (settings.gameMenuState == CharacterInfo.GameMenuState.LOCATIONS_MENU_SELECTED) {
				
				exitText.setBorderStyle(FlxTextBorderStyle.OUTLINE, FlxColor.BLACK, 3);
				locationsText.setBorderStyle(FlxTextBorderStyle.OUTLINE, FlxColor.BLACK, 3);
				mainMenuText.setBorderStyle(FlxTextBorderStyle.OUTLINE, FlxColor.BLACK, 3);
				resumeText.setBorderStyle(FlxTextBorderStyle.OUTLINE, FlxColor.BLACK, 3);
				
				for (i in 0...locationTexts.length) {
					
					if (i == locationsMenuSelectedIndex) {
						
						locationTexts[i].setBorderStyle(FlxTextBorderStyle.OUTLINE, bgColor.getLightened(), 3);
						
					} else {
						
						locationTexts[i].setBorderStyle(FlxTextBorderStyle.OUTLINE, FlxColor.BLACK, 3);
						
					}
					
				}
				
				if (FlxG.keys.anyJustPressed([UP, W])) {
					
					locationsMenuSelectedIndex -= 1;
					
				} else if (FlxG.keys.anyJustPressed([DOWN, S])) {
					
					locationsMenuSelectedIndex += 1;
					
				} else if (FlxG.keys.anyJustPressed([ENTER, SPACE])) {
					
					var locationSelected:String = locationTexts[locationsMenuSelectedIndex].text;
					
					if (locationSelected == "Back") {
						
						for (locationText in locationTexts)
							FlxTween.tween(locationText, {alpha: 0, y: locationsText.y}, 0.2);
						
						FlxTween.tween(this, {mainMenuTextYOffset: locationsText.y + locationsText.height}, 0.2);
						FlxTween.tween(this, {exitTextYOffset: locationsText.y + locationsText.height + mainMenuText.height}, 0.2);
						
						settings.gameMenuState = CharacterInfo.GameMenuState.LOCATIONS_SELECTED;
						
					} else {
						
						character.location = locationSelected;
						character.state = CharacterInfo.CharacterState.STATIONARY;
						settings.gameMenuState = CharacterInfo.GameMenuState.RESUME_SELECTED;
						FlxG.camera.fade(FlxColor.BLACK, 0.2, false, switchToLoadingState);
						
					}
					
				} else if (FlxG.keys.anyJustPressed([ESCAPE, BACKSPACE])) {
						
					for (locationText in locationTexts)
						FlxTween.tween(locationText, {alpha: 0, y: locationsText.y}, 0.2);
						
					FlxTween.tween(this, {mainMenuTextYOffset: locationsText.y + locationsText.height}, 0.2);
					FlxTween.tween(this, {exitTextYOffset: locationsText.y + locationsText.height + mainMenuText.height}, 0.2);
					
					settings.gameMenuState = CharacterInfo.GameMenuState.LOCATIONS_SELECTED;
					
				}
				
				locationsMenuSelectedIndex = FlxMath.wrap(locationsMenuSelectedIndex, 0, locationTexts.length - 1);
				
			} else if (settings.gameMenuState == CharacterInfo.GameMenuState.LOCATIONS_SELECTED) {
				
				exitText.setBorderStyle(FlxTextBorderStyle.OUTLINE, FlxColor.BLACK, 3);
				locationsText.setBorderStyle(FlxTextBorderStyle.OUTLINE, bgColor.getLightened(), 3);
				mainMenuText.setBorderStyle(FlxTextBorderStyle.OUTLINE, FlxColor.BLACK, 3);
				resumeText.setBorderStyle(FlxTextBorderStyle.OUTLINE, FlxColor.BLACK, 3);
				
				if (FlxG.keys.anyJustPressed([UP, W])) {
					
					settings.gameMenuState = CharacterInfo.GameMenuState.RESUME_SELECTED;
					
				} else if (FlxG.keys.anyJustPressed([DOWN, S])) {
					
					settings.gameMenuState = CharacterInfo.GameMenuState.MAIN_MENU_SELECTED;
					
				} else if (FlxG.keys.anyJustPressed([ENTER, SPACE])) {
					
					for (oldLocationText in locationTexts)
						remove(oldLocationText);
					
					locationTexts = new Array<FlxText>();
					
					for (locationString in character.unlockedLocations) {
						
						var newLocationText:FlxText = new FlxText(0, 0, locationString);
						newLocationText.alpha = 0.0;
						newLocationText.setFormat(null, 16, FlxColor.fromRGB(255, 215, 0), FlxTextAlign.CENTER, FlxTextBorderStyle.OUTLINE, FlxColor.BLACK);
						newLocationText.setBorderStyle(FlxTextBorderStyle.OUTLINE, FlxColor.BLACK, 3);
						newLocationText.x = FlxG.camera.scroll.x + ((FlxG.width - newLocationText.width) / 2);
						newLocationText.y = locationsText.y;
						add(newLocationText);
						locationTexts.push(newLocationText);
						
					}
					
					var backText:FlxText = new FlxText(0, 0, "Back");
					backText.alpha = 0.0;
					backText.setFormat(null, 16, FlxColor.fromRGB(255, 215, 0), FlxTextAlign.CENTER, FlxTextBorderStyle.OUTLINE, FlxColor.BLACK);
					backText.setBorderStyle(FlxTextBorderStyle.OUTLINE, FlxColor.BLACK, 3);
					backText.x = FlxG.camera.scroll.x + ((FlxG.width - backText.width) / 2);
					backText.y = locationsText.y;
					add(backText);
					locationTexts.push(backText);
					
					var height:Float = locationsText.y + locationsText.height;
					for (locationText in locationTexts) {
						
						FlxTween.tween(locationText, {alpha: 1.0, y: height}, 0.2);
						height += locationText.height;
						
					}
					
					FlxTween.tween(this, {mainMenuTextYOffset: height}, 0.2);
					FlxTween.tween(this, {exitTextYOffset: height + mainMenuText.height}, 0.2);
					
					locationsMenuSelectedIndex = 0;
					
					settings.gameMenuState = CharacterInfo.GameMenuState.LOCATIONS_MENU_SELECTED;
					
				} else if (FlxG.keys.anyJustPressed([ESCAPE, BACKSPACE])) {
					
					FlxTween.tween(exitText, {alpha: 0}, settings.menuSpeed);
					FlxTween.tween(this, {exitTextYOffset: FlxG.camera.scroll.y - exitText.height}, settings.menuSpeed);
					FlxTween.tween(locationsText, {alpha: 0}, settings.menuSpeed);
					FlxTween.tween(this, {locationsTextYOffset: FlxG.camera.scroll.y - locationsText.height}, settings.menuSpeed);
					FlxTween.tween(mainMenuText, {alpha: 0}, settings.menuSpeed);
					FlxTween.tween(this, {mainMenuTextYOffset: FlxG.camera.scroll.y - mainMenuText.height}, settings.menuSpeed);
					FlxTween.tween(resumeText, {alpha: 0}, settings.menuSpeed);
					FlxTween.tween(this, {resumeTextYOffset: FlxG.camera.scroll.y - resumeText.height}, settings.menuSpeed);
					character.state = CharacterInfo.CharacterState.STATIONARY;
					
				}
				
			} else if (settings.gameMenuState == CharacterInfo.GameMenuState.MAIN_MENU_SELECTED) {
				
				exitText.setBorderStyle(FlxTextBorderStyle.OUTLINE, FlxColor.BLACK, 3);
				locationsText.setBorderStyle(FlxTextBorderStyle.OUTLINE, FlxColor.BLACK, 3);
				mainMenuText.setBorderStyle(FlxTextBorderStyle.OUTLINE, bgColor.getLightened(), 3);
				resumeText.setBorderStyle(FlxTextBorderStyle.OUTLINE, FlxColor.BLACK, 3);
				
				if (FlxG.keys.anyJustPressed([UP, W])) {
					
					settings.gameMenuState = CharacterInfo.GameMenuState.LOCATIONS_SELECTED;
					
				} else if (FlxG.keys.anyJustPressed([DOWN, S])) {
					
					settings.gameMenuState = CharacterInfo.GameMenuState.EXIT_SELECTED;
					
				} else if (FlxG.keys.anyJustPressed([ENTER, SPACE])) {
					
					character.state = CharacterInfo.CharacterState.STATIONARY;
					character.location = "Mystery Select State";
					FlxG.camera.fade(FlxColor.BLACK, 0.2, false, switchToLoadingState);
					
				} else if (FlxG.keys.anyJustPressed([ESCAPE, BACKSPACE])) {
					
					FlxTween.tween(exitText, {alpha: 0}, settings.menuSpeed);
					FlxTween.tween(this, {exitTextYOffset: FlxG.camera.scroll.y - exitText.height}, settings.menuSpeed);
					FlxTween.tween(locationsText, {alpha: 0}, settings.menuSpeed);
					FlxTween.tween(this, {locationsTextYOffset: FlxG.camera.scroll.y - locationsText.height}, settings.menuSpeed);
					FlxTween.tween(mainMenuText, {alpha: 0}, settings.menuSpeed);
					FlxTween.tween(this, {mainMenuTextYOffset: FlxG.camera.scroll.y - mainMenuText.height}, settings.menuSpeed);
					FlxTween.tween(resumeText, {alpha: 0}, settings.menuSpeed);
					FlxTween.tween(this, {resumeTextYOffset: FlxG.camera.scroll.y - resumeText.height}, settings.menuSpeed);
					character.state = CharacterInfo.CharacterState.STATIONARY;
					
				}
				
			} else if (settings.gameMenuState == CharacterInfo.GameMenuState.RESUME_SELECTED) {
				
				exitText.setBorderStyle(FlxTextBorderStyle.OUTLINE, FlxColor.BLACK, 3);
				locationsText.setBorderStyle(FlxTextBorderStyle.OUTLINE, FlxColor.BLACK, 3);
				mainMenuText.setBorderStyle(FlxTextBorderStyle.OUTLINE, FlxColor.BLACK, 3);
				resumeText.setBorderStyle(FlxTextBorderStyle.OUTLINE, bgColor.getLightened(), 3);
				
				if (FlxG.keys.anyJustPressed([UP, W])) {
					
					settings.gameMenuState = CharacterInfo.GameMenuState.EXIT_SELECTED;
					
				} else if (FlxG.keys.anyJustPressed([DOWN, S])) {
					
					settings.gameMenuState = CharacterInfo.GameMenuState.LOCATIONS_SELECTED;
					
				} else if (FlxG.keys.anyJustPressed([ENTER, SPACE])) {
					
					FlxTween.tween(exitText, {alpha: 0}, settings.menuSpeed);
					FlxTween.tween(this, {exitTextYOffset: FlxG.camera.scroll.y - exitText.height}, settings.menuSpeed);
					FlxTween.tween(locationsText, {alpha: 0}, settings.menuSpeed);
					FlxTween.tween(this, {locationsTextYOffset: FlxG.camera.scroll.y - locationsText.height}, settings.menuSpeed);
					FlxTween.tween(mainMenuText, {alpha: 0}, settings.menuSpeed);
					FlxTween.tween(this, {mainMenuTextYOffset: FlxG.camera.scroll.y - mainMenuText.height}, settings.menuSpeed);
					FlxTween.tween(resumeText, {alpha: 0}, settings.menuSpeed);
					FlxTween.tween(this, {resumeTextYOffset: FlxG.camera.scroll.y - resumeText.height}, settings.menuSpeed);
					character.state = CharacterInfo.CharacterState.STATIONARY;
					
				} else if (FlxG.keys.anyJustPressed([ESCAPE, BACKSPACE])) {
					
					FlxTween.tween(exitText, {alpha: 0}, settings.menuSpeed);
					FlxTween.tween(this, {exitTextYOffset: FlxG.camera.scroll.y - exitText.height}, settings.menuSpeed);
					FlxTween.tween(locationsText, {alpha: 0}, settings.menuSpeed);
					FlxTween.tween(this, {locationsTextYOffset: FlxG.camera.scroll.y - locationsText.height}, settings.menuSpeed);
					FlxTween.tween(mainMenuText, {alpha: 0}, settings.menuSpeed);
					FlxTween.tween(this, {mainMenuTextYOffset: FlxG.camera.scroll.y - mainMenuText.height}, settings.menuSpeed);
					FlxTween.tween(resumeText, {alpha: 0}, settings.menuSpeed);
					FlxTween.tween(this, {resumeTextYOffset: FlxG.camera.scroll.y - resumeText.height}, settings.menuSpeed);
					character.state = CharacterInfo.CharacterState.STATIONARY;
					
				}
				
			}
			
		} else if (character.state == CharacterInfo.CharacterState.MOVING) {
			
			if (character.tween.finished) {
				
				character.state = CharacterInfo.CharacterState.STATIONARY;
				
			}
			
		} else if (character.state == CharacterInfo.CharacterState.STATIONARY) {
			
			exitTextYOffset = FlxG.camera.scroll.y - exitText.height;
			locationsTextYOffset = FlxG.camera.scroll.y - locationsText.height;
			mainMenuTextYOffset = FlxG.camera.scroll.y - mainMenuText.height;
			resumeTextYOffset = FlxG.camera.scroll.y - resumeText.height;
			
			if (FlxG.keys.anyPressed([DOWN, S])) {
				
				character.watsonSprite.facing = FlxObject.DOWN;
				character.watsonSprite.animation.play("down");
				character.tween = FlxTween.tween(character.watsonSprite, {x: character.watsonSprite.x, y: (character.watsonSprite.y + 36)}, character.speed);
				character.state = CharacterInfo.CharacterState.MOVING;
				
			} else if (FlxG.keys.anyPressed([LEFT, A])) {
				
				character.watsonSprite.facing = FlxObject.LEFT;
				character.watsonSprite.animation.play("left");
				character.tween = FlxTween.tween(character.watsonSprite, {x: (character.watsonSprite.x - 36), y: character.watsonSprite.y}, character.speed);
				character.state = CharacterInfo.CharacterState.MOVING;
				
			} else if (FlxG.keys.anyPressed([RIGHT, D])) {
				
				character.watsonSprite.facing = FlxObject.RIGHT;
				character.watsonSprite.animation.play("right");
				character.tween = FlxTween.tween(character.watsonSprite, {x: (character.watsonSprite.x + 36), y: character.watsonSprite.y}, character.speed);
				character.state = CharacterInfo.CharacterState.MOVING;
				
			} else if (FlxG.keys.anyPressed([UP, W])) {
				
				character.watsonSprite.facing = FlxObject.UP;
				character.watsonSprite.animation.play("up");
				character.tween = FlxTween.tween(character.watsonSprite, {x: character.watsonSprite.x, y: (character.watsonSprite.y - 36)}, character.speed);
				character.state = CharacterInfo.CharacterState.MOVING;
				
			} else if (FlxG.keys.anyJustPressed([ENTER, SPACE])) {
				
				var point:FlxPoint = new FlxPoint(0, 0);
				switch (character.watsonSprite.facing) {
					
					case FlxObject.DOWN:
						point.set((character.watsonSprite.x + 18), (character.watsonSprite.y + 18) + 36);
						
					case FlxObject.LEFT:
						point.set((character.watsonSprite.x + 18) - 36, (character.watsonSprite.y + 18));
						
					case FlxObject.RIGHT:
						point.set((character.watsonSprite.x + 18) + 36, (character.watsonSprite.y + 18));
						
					case FlxObject.UP:
						point.set((character.watsonSprite.x + 18), (character.watsonSprite.y + 18) - 36);
					
				}
				
				character.interactingWith = getSpriteAt(point, layer1);
				if (character.interactingWith == null)
					character.interactingWith = getSpriteAt(point, layer2);
					
				if (character.interactingWith != null) {
					
					character.nameText.text = character.interactingWith.prettyName;
					remove(talkingProp);
					talkingProp = new Prop(character.location, character.interactingWith.name, 0, 0);
					if (talkingProp.frameHeight > talkingProp.frameWidth) {
						
						talkingProp.scale.set((character.bubbleSprite.height - character.nameText.height) / talkingProp.frameHeight, (character.bubbleSprite.height - character.nameText.height) / talkingProp.frameHeight);
						talkingProp.updateHitbox();
						
					} else {
						
						talkingProp.scale.set((FlxG.width - character.bubbleSprite.width - 10) / talkingProp.frameWidth, (FlxG.width - character.bubbleSprite.width - 10) / talkingProp.frameWidth);
						talkingProp.updateHitbox();
						
					}
					
					add(talkingProp);
					
					var line:String = character.interactingWith.getNextLine();
					character.speechText.text = line;
					
					speechRect = new FlxRect(0, 0, 0, 0);
					speechRect.height = (character.bubbleSprite.height >= (talkingProp.height + character.nameText.height)) ? character.bubbleSprite.height : (talkingProp.height + character.nameText.height);
					speechRect.width = character.bubbleSprite.width + ((talkingProp.width >= character.nameText.width) ? talkingProp.width : character.nameText.width);
					speechRect.x = FlxG.camera.scroll.x + ((FlxG.width - speechRect.width) / 2);
					speechRect.y = FlxG.camera.scroll.y + FlxG.height + speechRectYOffset;
					
					character.bubbleSprite.x = speechRect.x + speechRect.width - character.bubbleSprite.width;
					character.bubbleSprite.y = speechRect.y + ((speechRect.height - character.bubbleSprite.height) / 2);
					
					character.speechText.x = character.bubbleSprite.x + 32;
					character.speechText.y = character.bubbleSprite.y + ((character.bubbleSprite.height - character.speechText.height) / 2);
					
					talkingProp.x = speechRect.x + ((((talkingProp.width >= character.nameText.width) ? talkingProp.width : character.nameText.width) - talkingProp.width) / 2);
					talkingProp.y = speechRect.y + ((speechRect.height - talkingProp.height - character.nameText.height) / 2);
					
					character.nameText.x = speechRect.x + ((((talkingProp.width >= character.nameText.width) ? talkingProp.width : character.nameText.width) - character.nameText.width) / 2);
					character.nameText.y = talkingProp.y + talkingProp.height;
					
					FlxTween.tween(this, {speechRectYOffset: -speechRect.height - 5}, 0.2);
					
				}
				character.state = CharacterInfo.CharacterState.INTERACTING;
				
			} else if (FlxG.keys.anyJustPressed([ESCAPE, BACKSPACE])) {
				
				var initialHeight:Float = FlxG.camera.scroll.y + ((FlxG.height - exitText.height - locationsText.height - mainMenuText.height - resumeText.height) / 2);
				FlxTween.tween(exitText, {alpha: 1}, settings.menuSpeed);
				FlxTween.tween(this, {exitTextYOffset: initialHeight + resumeText.height + locationsText.height + mainMenuText.height}, settings.menuSpeed);
				FlxTween.tween(locationsText, {alpha: 1}, settings.menuSpeed);
				FlxTween.tween(this, {locationsTextYOffset: initialHeight + resumeText.height}, settings.menuSpeed);
				FlxTween.tween(mainMenuText, {alpha: 1}, settings.menuSpeed);
				FlxTween.tween(this, {mainMenuTextYOffset: initialHeight + resumeText.height + locationsText.height}, settings.menuSpeed);
				FlxTween.tween(resumeText, {alpha: 1}, settings.menuSpeed);
				FlxTween.tween(this, {resumeTextYOffset: initialHeight}, settings.menuSpeed);
				character.state = CharacterInfo.CharacterState.MENU;
				
			} else {
				
				character.watsonSprite.animation.finish();
				switch (character.watsonSprite.facing) {
					
					case FlxObject.DOWN:
						character.watsonSprite.animation.frameIndex = 1;
						
					case FlxObject.LEFT:
						character.watsonSprite.animation.frameIndex = 4;
						
					case FlxObject.RIGHT:
						character.watsonSprite.animation.frameIndex = 7;
						
					case FlxObject.UP:
						character.watsonSprite.animation.frameIndex = 10;
					
				}
				
			}
			
		}
		
		if (character.watsonSprite.x >= danielProp.x && character.watsonSprite.y > danielProp.y) {
			
			danielProp.facing = FlxObject.DOWN;
			
		} else if (character.watsonSprite.x < danielProp.x && character.watsonSprite.y >= danielProp.y) {
			
			danielProp.facing = FlxObject.LEFT;
			
		} else if (character.watsonSprite.x > danielProp.x && character.watsonSprite.y <= danielProp.y) {
			
			danielProp.facing = FlxObject.RIGHT;
			
		} else if (character.watsonSprite.x <= danielProp.x && character.watsonSprite.y < danielProp.y) {
			
			danielProp.facing = FlxObject.UP;
			
		}
		
		switch (danielProp.facing) {
			
			case FlxObject.DOWN:
				danielProp.animation.frameIndex = 1;
				
			case FlxObject.LEFT:
				danielProp.animation.frameIndex = 4;
				
			case FlxObject.RIGHT:
				danielProp.animation.frameIndex = 7;
				
			case FlxObject.UP:
				danielProp.animation.frameIndex = 10;
			
		}
		
		if (character.tween != null && FlxG.collide(character.watsonSprite, layer2) || FlxG.collide(character.watsonSprite, layer1))
			character.tween.cancel();
		
		super.update(elapsed);
		
	}
	
}